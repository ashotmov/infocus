<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::group([], function () {
    Route::get("/", "HomeController@index")->name("home");
    Route::get("/tests", "HomeController@tests")->name("tests");
    Route::get("/classes", "HomeController@classes")->name("classes");
    Route::get("/tutoring", "HomeController@tutoring")->name("tutoring");
    Route::get("/contacts", "HomeController@contacts")->name("contacts");

    Route::post("/add-to-cart", "User\CartController@addToCart")->name("add-to-cart");
    Route::delete("/remove-from-cart/{id}", "User\CartController@removeFromCart")->where(["id" => "[0-9]+"])->name("remove-from-cart");
});

Route::group(["middleware" => "guest"], function () {
    Route::get("/sign-in", "AuthController@signInPage")->name("sign-in");
    Route::get("/sign-up", "AuthController@signUpPage")->name("sign-up");
    Route::get("/forgot-password", "AuthController@forgotPasswordPage")->name("forgot-password");


    Route::post("/sign-in", "AuthController@signIn")->name("auth.sign-in");
    Route::post("/sign-up", "AuthController@signUp")->name("auth.sign-up");
    Route::post("/forgot-password", "AuthController@forgotPassword")->name("auth.forgot-password");
});

Route::group(["middleware" => "auth"], function () {
    Route::get("/dashboard", function () {
        return (auth()->user()->is_admin) ? redirect()->route("admin.dashboard") : redirect()->route("user.dashboard");
    })->name("dashboard");
    Route::post("/sign-out", "AuthController@signOut")->name("auth.logout");
});

Route::group(["middleware" => ["auth", "admin"], "namespace" => "Admin", "prefix" => "admin"], function () {
    Route::get("/dashboard", "DashboardController@index")->name("admin.dashboard");
    Route::post("/dashboard/test-sheet-url/{type}", "DashboardController@testSheetUrl")->name("admin.test-sheet-url");

    Route::group(["prefix" => "tests"], function () {
        Route::get("/", "TestsController@index")->name("admin.tests");
        Route::get("/{id}/show", "TestsController@show")->name("admin.tests.show");
        Route::delete("/{id}", "TestsController@delete")->name("admin.tests.delete");
    });
});

Route::group(["middleware" => ["user"], "namespace" => "User", "prefix" => "user"], function () {
    Route::get("/dashboard", "DashboardController@index")->name("user.dashboard");

    Route::get("/checkout", "CheckoutController@index")->name("checkout");
    Route::post("/save-address", "CheckoutController@saveAddress")->name("user.save-address");
    Route::post("/order", "CheckoutController@order")->name("user.order");

    Route::group(["prefix", "tests"], function () {
        Route::post("/start/", "TestController@startTest")->name("user.tests.start");
        Route::get("/show/{product_id}/{try_number}", "TestController@showTest")->name("user.tests.show");
        Route::post("/submit/{product_id}/{try_number}/{question_index}", "TestController@submitAnswer")->name("user.tests.submit-answer");
        Route::get("/result/{product_id}/{try_number}", "TestController@result")->name("user.tests.result");

    });
});

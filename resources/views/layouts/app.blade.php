<!DOCTYPE html>
<html lang="{{config("app.locale")}}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="/css/normalize.css">
    @yield("styles")
    <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="/css/fixing.css">
    <title>@yield("title", config("app.name"))</title>
</head>
<body>

<div class="@yield("header-wrapper-class", "wrapper")">
    @include("components.header")
    @yield("after-header")
</div>

@yield("content")

<div class="@yield("footer-wrapper-class", "home__footer")">
    @include("components.footer")
</div>

@yield("after-footer")

@section("scripts")@show
<script src="/js/script.js"></script>
</body>
</html>

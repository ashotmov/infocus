@extends("layouts.app")
@section("header-wrapper-class", "result__header")
@section("footer-wrapper-class", "exam__footer")

@section("content")
    <section class="results-section">
        <div class="container">
            <div class="results-section-container">
                <div class="exam-results__title">ONLINE TESTS</div>
                <div class="exam-results__category exam-start__category">TESTS AND CLASSES</div>
                <div class="exam-start">
                    <div class="exam-start__title">
                        <div class="exam-start__title-inner">
                            <div class="try-1">
                                <div class="try-1__title">TRY 1</div>
                                <div class="try-1__inner">
                                    <span class="try-1__bottom">Action</span>
                                    <span class="try-1__bottom">Score</span>
                                </div>
                            </div>
                            <div class="try-1">
                                <div class="try-1__title">TRY 2</div>

                                <div class="try-1__inner">
                                    <span class="try-1__bottom">Action</span>
                                    <span class="try-1__bottom">Score</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    @foreach($user->data->products as
                     /** @var App\Models\OnlineTest\TestProduct $product */
                     $product)
                        <div class="exam-start__line">
                            <div class="exam-start__line-name">
                                {{$product->test->name}}
                            </div>

                            <div class="exam-start__line-left">
                                @if(!$product->try1->isExamStarted())
                                    <button
                                        data-number="1"
                                        data-product-id="{{$product->id}}"
                                        class="exam-start__line-start start-test-button">
                                        start
                                    </button>
                                @elseif(!$product->try1->is_pause)
                                    <button
                                        data-number="1"
                                        data-product-id="{{$product->id}}"
                                        class="exam-start__line-start continue-test-button">
                                        continue
                                    </button>
                                @else
                                    <button
                                        data-number="1"
                                        data-product-id="{{$product->id}}"
                                        class="exam-start__line-review review-test-button">
                                        review
                                    </button>
                                    <div class="exam-start__line-percent">
                                        <div class="percent-top">
                                            {{$product->try1->getResult()}}
                                        </div>
                                        <a class="percent-bottom">Details</a>
                                    </div>
                                @endif
                            </div>

                            @if ($product->try1->isCompleted())
                                <div class="exam-start__line-right">
                                    <a class="exam-start__line-review">review</a>
                                    <div class="exam-start__line-percent">
                                        <div class="percent-top">10%</div>
                                        <div class="percent-bottom">Details</div>
                                    </div>
                                </div>
                            @endif
                        </div>
                    @endforeach
                    {{--<div class="exam-start__line">
                        <div class="exam-start__line-name">CALIFORNIA CLINICAL MOCK EXAM 3</div>
                        <div class="exam-start__line-left">
                            <a class="exam-start__line-start">start</a>
                            <div class="exam-start__line-percent">
                                <div class="percent-top">10%</div>
                                <div class="percent-bottom">Detalis</div>
                            </div>

                        </div>
                        <div class="exam-start__line-right">

                            <a class="exam-start__line-review">review</a>
                            <div class="exam-start__line-percent">
                                <div class="percent-top">10%</div>
                                <div class="percent-bottom">Detalis</div>
                            </div>
                        </div>
                    </div>
                    <div class="exam-start__line">
                        <div class="exam-start__line-name">LAW & ETHICS MOCK EXAM 1</div>
                        <div class="exam-start__line-left">
                            <a class="exam-start__line-start">start</a>
                            <div class="exam-start__line-percent">
                                <div class="percent-top">10%</div>
                                <div class="percent-bottom">Detalis</div>
                            </div>

                        </div>
                        <div class="exam-start__line-right">

                            <a class="exam-start__line-review">start</a>
                            <div class="exam-start__line-percent">
                                <div class="percent-top">10%</div>
                                <div class="percent-bottom">Detalis</div>
                            </div>
                        </div>
                    </div>
                    <div class="exam-start__line">
                        <div class="exam-start__line-name">CALIFORNIA CLINICAL MOCK EXAM 2</div>
                        <div class="exam-start__line-left">
                            <a class="exam-start__line-start">continue</a>
                            <div class="exam-start__line-percent">
                                <div class="percent-top">10%</div>
                                <div class="percent-bottom">Detalis</div>
                            </div>

                        </div>
                        <div class="exam-start__line-right">

                            <a class="exam-start__line-review">review</a>
                            <div class="exam-start__line-percent">
                                <div class="percent-top">10%</div>
                                <div class="percent-bottom">Detalis</div>
                            </div>
                        </div>
                    </div>
                    <div class="exam-start__line">
                        <div class="exam-start__line-name">LAW & ETHICS MOCK EXAM 3</div>
                        <div class="exam-start__line-left">
                            <a class="exam-start__line-start">start</a>
                            <div class="exam-start__line-percent">
                                <div class="percent-top">10%</div>
                                <div class="percent-bottom">Detalis</div>
                            </div>

                        </div>
                        <div class="exam-start__line-right">

                            <a class="exam-start__line-review">review</a>
                            <div class="exam-start__line-percent">
                                <div class="percent-top">10%</div>
                                <div class="percent-bottom">Detalis</div>
                            </div>
                        </div>
                    </div>--}}
                </div>
            </div>
            <div class="results-section-container">
                <div class="exam-results__title">RECENT TRANSACTIONS</div>
                <div class="exam-results__category">ONLINE TESTS</div>
                <form action="" class="results__form">
                    <div class="results-form__title">
                        <div class="results__title-inner results__title-inner-top">DATE PURCHASED</div>
                        <div class="results__title-inner
                            results__title-inner-top">Online Tests
                        </div>
                        <div class="results__title-inner results__title-inner-top">
                            TEST TAKEN (TIMES)
                        </div>
                        <div class="results__title-inner results__title-inner-top">
                            TEST EXPIRATION DATE
                        </div>
                    </div>

                    <div class="classes__exam-input-field">
                        <label class="law__exam-label classes__first-label">
                            <input type="checkbox" class="classes__exam3" name="checkbox">
                            <span class="exam__label-check result-check">
                                <img src="/img/tests/shape_1.png" alt="Checked" class="check__img">
                            </span>
                            <div class="results-form__title">
                                <div class="results__title-inner">Nov 3, 2019</div>
                                <div class="results__title-inner">LAW & ETHICS MOCK EXAM 3</div>
                                <div class="results__title-inner">0 of 2</div>
                                <div class="results__title-inner">Feb 8, 2019</div>
                            </div>

                        </label>
                    </div>
                    <div class="classes__exam-input-field">
                        <label class="law__exam-label classes__first-label">
                            <input type="checkbox" class="classes__exam3" name="checkbox">
                            <span class="exam__label-check result-check">
                                <img src="/img/tests/shape_1.png" alt="Checked" class="check__img">
                            </span>
                            <div class="results-form__title">
                                <div class="results__title-inner">Nov 3, 2019</div>
                                <div class="results__title-inner">CALIFORNIA CLINICAL MOCK EXAM 2</div>
                                <div class="results__title-inner">0 of 2</div>
                                <div class="results__title-inner">Feb 8, 2019</div>
                            </div>
                        </label>
                    </div>
                    <div class="classes__exam-input-field">
                        <label class="law__exam-label classes__first-label">
                            <input type="checkbox" class="classes__exam3" name="checkbox">
                            <span class="exam__label-check result-check">
                                <img src="/img/tests/shape_1.png" alt="Checked" class="check__img">
                            </span>
                            <div class="results-form__title">
                                <div class="results__title-inner">Nov 3, 2019</div>
                                <div class="results__title-inner">JohnDoe</div>
                                <div class="results__title-inner">0 of 2</div>
                                <div class="results__title-inner">Feb 8, 2019</div>
                            </div>

                        </label>
                    </div>
                    <div class="classes__exam-input-field">
                        <label class="law__exam-label classes__first-label">
                            <input type="checkbox" class="classes__exam3" name="checkbox">
                            <span class="exam__label-check result-check">
                                <img src="/img/tests/shape_1.png" alt="Checked" class="check__img">
                            </span>
                            <div class="results-form__title">
                                <div class="results__title-inner">Nov 3, 2019</div>
                                <div class="results__title-inner">LAW & ETHICS MOCK EXAM 2</div>
                                <div class="results__title-inner">0 of 2</div>
                                <div class="results__title-inner">Feb 8, 2019</div>
                            </div>

                        </label>
                    </div>
                </form>
                <div class="result-buttons">
                    <label class="law__exam-label select-all-label">
                        <input type="checkbox" class="classes__exam3" name="checkbox">
                        <span class="exam__label-check result-check">
                            <img src="/img/tests/shape_1.png" alt="Checked" class="check__img">
                        </span>

                        <div class="select-all">Select All</div>
                    </label>

                </div>
                <a href="" class="refund">Refund</a>
                <div class="cancellation">

                    <a href="" class="cancellation__inner">CANCELLATION POLICY:</a>
                </div>
            </div>

        </div>
        <div class="results-section-container">
            <div class="exam-results__title">RECENT TRANSACTIONS</div>
            <div class="exam-results__category">LIVE GROUP CLASSES</div>
            <form action="" class="results__form">
                <div class="results-form__title enrolled-for">
                    <div class="results__title-inner results__title-inner-top">
                        DATE PURCHASED
                    </div>

                    <div class="results__title-inner results__title-inner-top">
                        Classes
                    </div>
                    <div class="results__title-inner results__title-inner-top">
                        ENROLLED FOR
                    </div>
                </div>

                <div class="classes__exam-input-field">
                    <label class="law__exam-label classes__first-label">
                        <input type="checkbox" class="classes__exam3" name="checkbox">
                        <span class="exam__label-check result-check">
                            <img src="/img/tests/shape_1.png" alt="Checked" class="check__img">
                        </span>
                        <div class="results-form__title enrolled-for">
                            <div class="results__title-inner">Nov 3, 2019</div>
                            <div class="results__title-inner">CCE Mock Exam 2</div>
                            <div class="results__title-inner">Feb 10, 2019</div>
                        </div>

                    </label>
                </div>
                <div class="classes__exam-input-field">
                    <label class="law__exam-label classes__first-label">
                        <input type="checkbox" class="classes__exam3" name="checkbox">
                        <span class="exam__label-check result-check">
                            <img src="/img/tests/shape_1.png" alt="Checked" class="check__img">
                        </span>
                        <div class="results-form__title enrolled-for">
                            <div class="results__title-inner">Nov 3, 2019</div>
                            <div class="results__title-inner">CCE Mock Exam 2</div>
                            <div class="results__title-inner">Feb 10, 2019</div>

                        </div>

                    </label>
                </div>
                <div class="classes__exam-input-field">
                    <label class="law__exam-label classes__first-label">
                        <input type="checkbox" class="classes__exam3" name="checkbox">
                        <span class="exam__label-check result-check">
                            <img src="/img/tests/shape_1.png" alt="Checked" class="check__img">
                        </span>
                        <div class="results-form__title enrolled-for">
                            <div class="results__title-inner">Nov 3, 2019</div>
                            <div class="results__title-inner">CCE Mock Exam 2</div>
                            <div class="results__title-inner">Feb 10, 2019</div>

                        </div>

                    </label>
                </div>
                <div class="classes__exam-input-field">
                    <label class="law__exam-label classes__first-label">
                        <input type="checkbox" class="classes__exam3" name="checkbox">
                        <span class="exam__label-check result-check">
                            <img src="/img/tests/shape_1.png" alt="Checked" class="check__img">
                        </span>
                        <div class="results-form__title enrolled-for">
                            <div class="results__title-inner">Nov 3, 2019</div>
                            <div class="results__title-inner">CCE Mock Exam 2</div>
                            <div class="results__title-inner"> Feb 10, 2019</div>

                        </div>
                    </label>
                </div>

            </form>
            <div class="result-buttons">
                <label class="law__exam-label select-all-label">
                    <input type="checkbox" class="classes__exam3" name="checkbox">
                    <span class="exam__label-check result-check">
                        <img src="/img/tests/shape_1.png" alt="Checked" class="check__img">
                    </span>

                    <div class="select-all">Select All</div>
                </label>

            </div>
            <a href="" class="refund">Refund</a>
            <div class="cancellation">

                <a href="" class="cancellation__inner">CANCELLATION POLICY:</a>
            </div>
        </div>
    </section>
@stop

@section("after-footer")
    <div class="wrapper-popup">
    </div>

    <div class="popup-dashboard">
        <div class="exam-1__inner dashboard-inner">
            <div class="exam-a__top dashboard-top">
                <div class="exam-a__top-left">
                    <div class="exam-a__top-title dashboard-table__title">
                        EXAM: CALIFORNIA CLINICAL MOCK EXAM 1
                    </div>
                </div>
            </div>
            <div class="exam-start__title">
                <div class="exam-start__title-inner dashboard-start__title-inner">
                    <div class="try-1">
                        <div class="try-1__title">TRY 1</div>
                    </div>
                    <div class="try-1">
                        <div class="try-1__title">TRY 2</div>
                    </div>
                </div>
            </div>
            <table class="table dashboard-table">
                <div class="caption">SUMMARY SCORE</div>
                <tr class="table-line ">
                    <td class="table-line__inner uppercase">DATE STARTED</td>
                    <td class="table-line__inner">Nov 1, 2018</td>
                    <td class="table-line__inner">Nov 1, 2018</td>

                </tr>
                <tr class="table-line dashboard-table__line">
                    <td class="table-line__inner uppercase">TIME STARTED</td>
                    <td class="table-line__inner">10:35a PT</td>
                    <td class="table-line__inner">12:15 PT</td>
                </tr>
                <tr class="table-line dashboard-table__line">
                    <td class="table-line__inner uppercase">DATE COMPLETED</td>
                    <td class="table-line__inner">Nov 2, 2018</td>
                    <td class="table-line__inner">Nov 1, 2018</td>
                </tr>
                <tr class="table-line">
                    <td class="table-line__inner uppercase">TIME COMPLETED</td>
                    <td class="table-line__inner">12:13p PT</td>
                    <td class="table-line__inner">11:13p PT</td>
                </tr>
                <tr class="table-line">
                    <td class="table-line__inner uppercase">DURATION</td>
                    <td class="table-line__inner">55:35 mins</td>
                    <td class="table-line__inner">30:35 mins</td>
                </tr>
                <tr class="table-line">
                    <td class="table-line__inner uppercase">ANSWERED</td>
                    <td class="table-line__inner">20</td>
                    <td class="table-line__inner">13</td>
                </tr>
                <tr class="table-line">
                    <td class="table-line__inner uppercase">CORRECT</td>
                    <td class="table-line__inner">11</td>
                    <td class="table-line__inner">13</td>
                </tr>
                <tr class="table-line">
                    <td class="table-line__inner uppercase">INCORRECT</td>
                    <td class="table-line__inner">9</td>
                    <td class="table-line__inner">13</td>
                </tr>
                <tr class="table-line total-bold">
                    <td class="table-line__inner uppercase ">TOTAL SCORE</td>
                    <td class="table-line__inner ">50%</td>
                    <td class="table-line__inner ">60%</td>
                </tr>
            </table>
            <table class="table dashboard-table2">
                <div class="caption">CORRECT ANSWERS</div>
                <tr class="table-line">
                    <td class="table-line__inner uppercase">INCORRECT</td>
                    <td class="table-line__inner">9</td>
                    <td class="table-line__inner">13</td>
                </tr>
                <tr class="table-line">
                    <td class="table-line__inner uppercase">INCORRECT</td>
                    <td class="table-line__inner">9</td>
                    <td class="table-line__inner">13</td>
                </tr>
            </table>
        </div>
    </div>

    <div class="popup-choose-exam">
        <div class="popup-choose-exam__inner">

            <h3 class="choose-exam__title">Please Choose in which Mode
                you like to take this Online Test:</h3>
            <form action="{{route("user.tests.start")}}" method="post" class="choose-exam__form">
                @csrf
                <input type="hidden" name="try_number" class="choose-form__number">
                <input type="hidden" name="product_id" class="choose-form__product-id">
                <label class="choose-exam__label">
                    <input type="radio" class="choose-exam__input" name="mode" value="exam">
                    <span class="exam-mode">Exam Mode</span>
                </label>
                <label class="choose-exam__label">
                    <input type="radio" class="choose-exam__input" name="mode" value="study">
                    <span class="exam-mode">Study Mode</span>
                </label>
                <div class="choose-exam__button-field">
                    <button type="button" class="choose-exam__cancel">cancel</button>
                    <button type="submit" class="choose-exam__ok">ok</button>
                </div>
            </form>
        </div>
    </div>
@stop


@extends("layouts.app")
@section("title", "Dashboard")

@section("content")
    <section class="results-section">
        <div class="container">
            <div class="results-section-container">
                <div class="exam-results__title">RECENT TRANSACTIONS</div>
                <div class="exam-results__category">ONLINE TESTS</div>
                <form action="" class="results__form">
                    <div class="results-form__title">
                        <div class="results__title-inner results__title-inner-top">date</div>
                        <div class="results__title-inner
                        results__title-inner-top">user
                        </div>
                        <div class="results__title-inner
                        results__title-inner-top">online test purchased
                        </div>
                        <div class="results__title-inner
                        results__title-inner-top">amount
                        </div>
                    </div>

                    <div class="classes__exam-input-field">
                        <label class="law__exam-label classes__first-label">
                            <input type="checkbox" class="classes__exam3" name="checkbox">
                            <span class="exam__label-check result-check">
                                <img src="/img/tests/shape_1.png" alt="Checked" class="check__img"></span>
                            <div class="results-form__title">
                                <div class="results__title-inner">Nov 3, 2019</div>
                                <div class="results__title-inner">JohnDoe</div>
                                <div class="results__title-inner">CCE Mock Exam 2</div>
                                <div class="results__title-inner">$35.00</div>
                            </div>

                        </label>
                    </div>
                    <div class="classes__exam-input-field">
                        <label class="law__exam-label classes__first-label">
                            <input type="checkbox" class="classes__exam3" name="checkbox">
                            <span class="exam__label-check result-check"> 
                                <img src="/img/tests/shape_1.png" alt="Checked" class="check__img">
                            </span>
                            <div class="results-form__title">
                                <div class="results__title-inner">Nov 3, 2019</div>
                                <div class="results__title-inner">JohnDoe</div>
                                <div class="results__title-inner">CCE Mock Exam 2</div>
                                <div class="results__title-inner">$35.00</div>
                            </div>

                        </label>
                    </div>
                    <div class="classes__exam-input-field">
                        <label class="law__exam-label classes__first-label">
                            <input type="checkbox" class="classes__exam3" name="checkbox">
                            <span class="exam__label-check result-check">
                                <img src="/img/tests/shape_1.png" alt="Checked" class="check__img">
                            </span>
                            <div class="results-form__title">
                                <div class="results__title-inner">Nov 3, 2019</div>
                                <div class="results__title-inner">JohnDoe</div>
                                <div class="results__title-inner">CCE Mock Exam 2</div>
                                <div class="results__title-inner">$35.00</div>
                            </div>

                        </label>
                    </div>
                    <div class="classes__exam-input-field">
                        <label class="law__exam-label classes__first-label">
                            <input type="checkbox" class="classes__exam3" name="checkbox">
                            <span class="exam__label-check result-check">
                                <img src="/img/tests/shape_1.png" alt="Checked" class="check__img">
                            </span>
                            <div class="results-form__title">
                                <div class="results__title-inner">Nov 3, 2019</div>
                                <div class="results__title-inner">JohnDoe</div>
                                <div class="results__title-inner">CCE Mock Exam 2</div>
                                <div class="results__title-inner">$35.00</div>
                            </div>

                        </label>
                    </div>

                </form>
                <div class="result-buttons">
                    <label class="law__exam-label select-all-label">
                        <input type="checkbox" class="classes__exam3" name="checkbox">
                        <span class="exam__label-check result-check">
                            <img src="/img/tests/shape_1.png" alt="Checked" class="check__img">
                        </span>

                        <div class="select-all">Select All</div>

                    </label>

                </div>
                <a href="" class="refund">Refund</a>
            </div>
            <div class="results-section-container">
                <div class="exam-results__title">RECENT TRANSACTIONS</div>
                <div class="exam-results__category">ONLINE TESTS</div>
                <form action="" class="results__form">
                    <div class="results-form__title">
                        <div class="results__title-inner results__title-inner-top">date</div>
                        <div class="results__title-inner results__title-inner-top">user</div>
                        <div class="results__title-inner results__title-inner-top">class purchased</div>
                        <div class="results__title-inner results__title-inner-top">amount</div>
                    </div>

                    <div class="classes__exam-input-field">
                        <label class="law__exam-label classes__first-label">
                            <input type="checkbox" class="classes__exam3" name="checkbox">
                            <span class="exam__label-check result-check">
                                <img src="/img/tests/shape_1.png" alt="Checked" class="check__img">
                            </span>
                            <div class="results-form__title">
                                <div class="results__title-inner">Nov 3, 2019</div>
                                <div class="results__title-inner">JohnDoe</div>
                                <div class="results__title-inner">CCE Mock Exam 2</div>
                                <div class="results__title-inner">$100</div>
                            </div>

                        </label>
                    </div>
                    <div class="classes__exam-input-field">
                        <label class="law__exam-label classes__first-label">
                            <input type="checkbox" class="classes__exam3" name="checkbox">
                            <span class="exam__label-check result-check">
                                <img src="/img/tests/shape_1.png" alt="Checked" class="check__img">
                            </span>
                            <div class="results-form__title">
                                <div class="results__title-inner">Nov 3, 2019</div>
                                <div class="results__title-inner">JohnDoe</div>
                                <div class="results__title-inner">CCE Mock Exam 2</div>
                                <div class="results__title-inner">$100</div>
                            </div>

                        </label>
                    </div>
                    <div class="classes__exam-input-field">
                        <label class="law__exam-label classes__first-label">
                            <input type="checkbox" class="classes__exam3" name="checkbox">
                            <span class="exam__label-check result-check">
                                <img src="/img/tests/shape_1.png" alt="Checked" class="check__img">
                            </span>
                            <div class="results-form__title">
                                <div class="results__title-inner">Nov 3, 2019</div>
                                <div class="results__title-inner">JohnDoe</div>
                                <div class="results__title-inner">CCE Mock Exam 2</div>
                                <div class="results__title-inner">$100</div>
                            </div>

                        </label>
                    </div>
                    <div class="classes__exam-input-field">
                        <label class="law__exam-label classes__first-label">
                            <input type="checkbox" class="classes__exam3" name="checkbox">
                            <span class="exam__label-check result-check">
                                <img src="/img/tests/shape_1.png" alt="Checked" class="check__img">
                            </span>
                            <div class="results-form__title">
                                <div class="results__title-inner">Nov 3, 2019</div>
                                <div class="results__title-inner">JohnDoe</div>
                                <div class="results__title-inner">CCE Mock Exam 2</div>
                                <div class="results__title-inner">$100</div>
                            </div>

                        </label>
                    </div>

                </form>
                <div class="result-buttons">

                    <a href="" class="select-all">Select All</a>
                </div>
                <a href="" class="refund">Refund</a>
            </div>
            <div class="results-section-container">
                <div class="exam-results__title">GENERAL SETTINGS</div>
                <div class="exam-results__category">Exam Data Links (Google Sheets):</div>
                <form action="{{route("admin.test-sheet-url", 1)}}" method="post" class="general-settings__form">
                    @csrf
                    <div class="general-settings__label">
                        <div class="general-settings__text">Law and Ethics Exam:</div>
                        <input type="text" name="url" class="general-settings__input">
                        <button type="submit" class="refund">Update</button>
                    </div>
                </form>
                <form action="" class="general-settings__form">
                    <div class="general-settings__label">
                        <div class="general-settings__text">California Clinical Exam:</div>
                        <input type="text" class="general-settings__input">
                        <button type="submit" class="refund">Update</button>
                    </div>
                </form>
                <form action="" class="general-settings__form">
                    <div class="exam-results__category ">Test Length:</div>
                    <div class="general-settings__label ">
                        <div class="general-settings__inner-name">Law and Ethics Exam:</div>
                        <input type="number" class="general-settings__inner-input">
                        <div class="general-settings__inner-hour">hour(s)</div>
                        <input type="number" class="general-settings__inner-input">
                        <div class="general-settings__inner-min">mins</div>
                    </div>
                    <div class="general-settings__label ">
                        <div class="general-settings__inner-name">Law and Ethics Exam:</div>
                        <input type="number" class="general-settings__inner-input">
                        <div class="general-settings__inner-hour">hour(s)</div>
                        <input type="number" class="general-settings__inner-input">
                        <div class="general-settings__inner-min">mins</div>
                    </div>
                    <a href="" class="refund">Update</a>
                </form>
            </div>
        </div>
    </section>
@stop

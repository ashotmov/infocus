@extends("layouts.app")

@section("content")
    <h1 style="text-align: center">Tests</h1>
    <section style="margin: 100px auto;width: 80%">
        <table style="width: 100%">
            <thead>
            <tr>
                <td>ID</td>
                <td>Name</td>
                <td>N</td>
                <td>Price</td>
                <td>Actions</td>
            </tr>
            </thead>
            <tbody>
            @foreach($tests as $test)
                <tr>
                    <td>#{{$test->id}}</td>
                    <td>{{$test->name}}</td>
                    <td>{{$test->questions()->count()}}</td>
                    <td>${{$test->price}}</td>
                    <td>
                        <a href="{{route("admin.tests.show", $test->id)}}">show</a>
                        <form action="{{route("admin.tests.delete", $test->id)}}" method="post">
                            @csrf
                            @method("delete")
                            <button type="submit">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </section>
@stop

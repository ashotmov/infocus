@extends("layouts.app")

@section("content")
    <div style="padding-left: 100px">
        <h1>Test #{{$test->id}}</h1>
        <h2>{{$test->name}}</h2>
        <p>
            <strong>Type: </strong>
            @switch($test->type)
                @case(1)
                Law & Ethics Exam
                @break
                @case(2)
                California Clinical Exam
                @break
            @endswitch
        </p>
        <p>
            <strong>Questions: </strong> {{$test->questions()->count()}}
        </p>
        <p>
            <strong>Price: </strong> ${{$test->price}}
        </p>

        <section>
            @if ($question)
                <div>
                    @if ($previous)
                        <a href="{{route("admin.tests.show", $test->id)."?question=$previous"}}">Previous</a>
                    @endif

                    <strong>Question N{{$question->number}}</strong>

                    @if($next)
                        <a href="{{route("admin.tests.show", $test->id)."?question=$next"}}">Next</a>
                    @endif
                </div>

                <div>
                    <p>{{{$question->question}}}</p>

                    <p>answer: <strong>{{$question->answer}}</strong></p>

                    <ul>
                        @foreach($question->variants as $variant)
                            <li>
                                <p><strong>{{$variant->key}}:</strong> {{$variant->answer}}</p>
                                <p>{{$variant->rationale}}</p>
                            </li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </section>
    </div>
@stop

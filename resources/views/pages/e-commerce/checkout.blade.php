@extends("layouts.app")
@section("header-wrapper-class", "checkout__header")
@section("footer-wrapper-class", "checkout__footer")
@section("after-header")
    <h2 class="online-test">Checkout</h2>
@stop

@section("content")
    <section class="checkout__center">
        <div class="container">
            <form action="{{route("user.save-address")}}" method="post" class="billing-adress">
                @csrf
                <h2 class="billing__title">
                    Billing Address
                </h2>
                <div class="name__field">
                    <label class="register__label checkout__label">
                        <div class="register__title">
                            First Name<span class="register__title-after">*</span>
                        </div>
                        <input class="register__input" name="first_name" type="text"
                               value="{{old("first_name") ?? $user->data->first_name}}">
                        @error("first_name")
                        <div class="register__help-block">{{$message}}</div>
                        @enderror
                    </label>
                    <label class="register__label checkout__label">
                        <div class="register__title">
                            Last Name<span class="register__title-after">*</span>
                        </div>
                        <input class="register__input" name="last_name" type="text"
                               value="{{old("last_name") ?? $user->data->last_name}}">
                        @error("last_name")
                        <div class="register__help-block">{{$message}}</div>
                        @enderror
                    </label>
                </div>
                <label class="register__label checkout__label">
                    <div class="register__title">
                        Address <span class="register__title-after">*</span>
                    </div>
                    <input class="register__input" name="address" type="text" value="{{old("address")}}"
                           placeholder="Street Address">
                    @error("address")
                    <div class="register__help-block">{{$message}}</div>
                    @enderror
                </label>
                <label class="register__label checkout__label">
                    <input class="register__input" name="additional_address" type="text"
                           value="{{old("additional_address")}}" placeholder="Apartment, suite, unite etc. (optional)">
                    @error("additional_address")
                    <div class="register__help-block">{{$message}}</div>
                    @enderror
                </label>
                <label class="register__label checkout__label">
                    <div class="register__title">
                        Town / City<span class="register__title-after">*</span>
                    </div>
                    <input class="register__input" name="town_city" type="text" value="{{old("town_city")}}">
                    @error("town_city")
                    <div class="register__help-block">{{$message}}</div>
                    @enderror
                </label>
                <label class="register__label checkout__label">
                    <div class="register__title">Postcode / Zip</div>
                    <input class="register__input" name="postcode" type="text" value="{{old("postcode")}}">
                    @error("postcode")
                    <div class="register__help-block">{{$message}}</div>
                    @enderror
                </label>
                <label class="register__label checkout__label">
                    <div class="register__title">
                        Email Address<span class="register__title-after">*</span>
                    </div>
                    <input class="register__input" name="email" type="email" value="{{old("email") ?? $user->email}}">
                    @error("email")
                    <div class="register__help-block">{{$message}}</div>
                    @enderror
                </label>
                <label class="register__label checkout__label">
                    <div class="register__title">
                        Phone<span class="register__title-after">*</span>
                    </div>
                    <input class="register__input phone-input" name="phone" type="tel"
                           value="{{old("phone") ?? $user->data->mobile_phone}}">
                    @error("phone")
                    <div class="register__help-block">{{$message}}</div>
                    @enderror
                </label>
                <label class="register__label order__label">
                    <div class="register__title">Order Notes</div>
                    <textarea maxlength="180" name="order_notes"
                              class="get-in-touch__message-textarea get-in-touch__input register__textarea"
                              id="get-in-touch-message"></textarea>
                    @error("order_notes")
                    <div class="register__help-block">{{$message}}</div>
                    @enderror
                </label>
                <button class="register__button">Save Address</button>
            </form>

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="your-order">
                <h2 class="billing__title">
                    Your order
                </h2>
                <div class="your-order__inner">
                    @php
                        $total = 0;
                    @endphp
                    @foreach(session()->get("cart", []) as $product)
                        @php
                            $total += $product["quantity"] * $product["price"];
                        @endphp
                        <div class="order-first">
                            <div class="order-first__text">{{$product["name"]}}</div>
                            <div class="order-first__price">${{$product["quantity"] * $product["price"]}}</div>
                        </div>
                    @endforeach
                    <div class="order-first order-total">
                        <div class="order-first__text ">Cart Subtotal</div>
                        <div class="order-first__price order-total__price">${{$total}}</div>
                    </div>
                </div>
                <div class="credit__field">
                    <form action="{{route("user.order")}}" method="post" class="credit-form">
                        @csrf
                        <label class="credit-label">
                            <div class="credit-label-top">
                                <input type="radio" name="credit-card" class="credit-input">
                                <span class="credit-name">Credit/Debit Card</span>
                                <span class="credit-img__field">
                                    <img src="/img/checkout/credit-icons.svg" alt="Credit Icons" class="credit-img">
                                </span>
                            </div>

                            <div class="debit-card">
                                <div class="card-type">Card Type</div>
                                <select id="card-select" class="register__input debit-card__inner">
                                    <option value="">American Express</option>
                                    <option value="Discover">Discover</option>
                                    <option value="JCB">JCB</option>
                                    <option value="Mastercard">Mastercard</option>
                                    <option value="Visa">Visa</option>
                                </select>

                                <label class="register__label card-type__label">
                                    <div class="card-type">Card No.</div>
                                    <input class="register__input debit-card__inner" type="text">
                                </label>
                                <div class="name__field card-type__field">
                                    <label for="" class="register__label checkout__label">
                                        <div class="card-type">CVV No.</div>
                                        <input class="register__input debit-card__inner" type="text">
                                    </label>
                                    <label class="register__label checkout__label">
                                        <div class="card-type">Expiry Date</div>
                                        <input class="register__input" type="text">
                                    </label>
                                </div>
                                <label class="register__label card-type__label1">
                                    <div class="card-type">Amount($)</div>
                                    <input class="register__input debit-card__inner" disabled type="text"
                                           placeholder="{{$total}}">
                                </label>
                            </div>
                        </label>
                        <label class="credit-label-top paypal-label">
                            <input type="radio" name="credit-card" class="credit-input">
                            <span class="credit-name paypal-name">Paypal</span>
                            <span class="paypal-img__field">
                                <img src="/img/checkout/credit-icons1.svg" alt="Credit Icons" class="credit-img">
                            </span>
                        </label>
                        <div class="credit-accept">
                            <input type="checkbox" id="credit-accept-input" name="agree">
                            <label class="read" for="credit-accept-input">
                                <span class="exam__label-check credit-check">
                                    <img src="/img/tests/shape_1.png" alt="Checked" class="check__img">
                                </span>
                                <div class="credit-accept__text">
                                    I have read and accept the Terms & Conditions
                                </div>
                            </label>
                            @error("agree")
                            <div>
                                some error text for checkbox
                            </div>
                            @enderror
                        </div>
                        <button type="submit" class="register__button">Save & Pay Now</button>
                    </form>
                </div>
            </div>
        </div>
    </section>
@stop

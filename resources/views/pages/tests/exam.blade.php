@extends("layouts.app")
@section("header-wrapper-class", "exam__header")
@section("footer-wrapper-class", "exam__footer")

@section("content")
    @php
        /**
         * @var App\Models\OnlineTest\TestTry $try
         */
    @endphp
    <section class="exam-a__section">
        <div class="container">
            <div class="exam-a__top">
                <div class="exam-a__top-left">
                    <div class="exam-a__top-title">{{$try->test->name}}</div>
                    <div class="exam-a__top-category">Category: {{$try->current->question->category}}</div>
                </div>
                <div class="exam-a__top-right">
                    <div class="exam-a__top-time">Elapsed Time: {{$try->getTimeLeft()}}</div>
                    <label class="exam-a__top-end">
                        <input type="submit"
                               name="end"
                               value="end"
                               class="visually-hidden"
                               form="answer-submit-form">
                        end
                    </label>
                    <div class="go-to-question">
                        <div class="go-to-question__inner">
                            <h3 class="go-to-question__title">Go to Question #</h3>
                            <ul class="next-preview">
                                <li class="next-preview__inner">
                                    <label class="next-preview__link">
                                        <input type="submit"
                                               name="to_first"
                                               value="to_first"
                                               class="visually-hidden"
                                               form="answer-submit-form">
                                        |<<
                                    </label>
                                </li>
                                <li class="next-preview__inner">
                                    <label class="next-preview__link">
                                        <input type="submit"
                                               name="prev_star"
                                               value="prev start"
                                               class="visually-hidden"
                                               form="answer-submit-form">
                                        &lt;
                                        <span class="star">
                                            <img src="/img/exam-a/star-fill.svg" alt="star" class="star-img">
                                        </span>
                                    </label>
                                </li>
                                <li class="next-preview__inner">
                                    <label class="preview-label">
                                        <input type="number"
                                               class="preview-input"
                                               name="question_number"
                                               form="answer-submit-form"
                                               min="1"
                                               max="{{$try->questions->count()}}">
                                        <input type="submit"
                                               name="to_number"
                                               class="next-preview__link"
                                               value="GO"
                                               form="answer-submit-form">
                                    </label>
                                </li>
                                <li class="next-preview__inner">
                                    <label class="next-preview__link">
                                        <input type="submit"
                                               name="next_star"
                                               value="next_star"
                                               class="visually-hidden"
                                               form="answer-submit-form">
                                        <span class="star">
                                            <img src="/img/exam-a/star-fill.svg" alt="star" class="star-img">
                                        </span>&gt;
                                    </label>
                                </li>
                                <li class="next-preview__inner">
                                    <label class="next-preview__link">
                                        <input type="submit"
                                               name="to_last"
                                               value="to_last"
                                               class="visually-hidden"
                                               form="answer-submit-form">
                                        >>|
                                    </label>
                                </li>
                                <div class="mark-star__field">
                                    <input type="checkbox"
                                           id="star-mark"
                                           name="star"
                                           form="answer-submit-form"
                                           class="mark-star__checkbox"{{$try->current->stared ? " checked" : ""}}>
                                    <label for="star-mark" class="for-star-mark">
                                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg"
                                             xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                             viewBox="0 0 42.1 41.4" style="enable-background:new 0 0 42.1 41.4;"
                                             xml:space="preserve">
                                        <style type="text/css">
                                            .st0 {
                                                stroke: #ED8A19;
                                                stroke-miterlimit: 10;
                                            }
                                        </style>

                                            <path class="st0"
                                                  d="M23.2,4.6l4.3,8.8c0.3,0.6,0.9,1,1.6,1.1l9.7,1.4c1.7,0.2,2.4,2.3,1.1,3.5l-7,6.8c-0.5,0.5-0.7,1.2-0.6,1.8 l1.7,9.6c0.3,1.7-1.5,3-3,2.2l-8.7-4.5c-0.6-0.3-1.3-0.3-1.9,0l-8.7,4.5c-1.5,0.8-3.3-0.5-3-2.2l1.7-9.6c0.1-0.7-0.1-1.4-0.6-1.8 l-7-6.8c-1.2-1.2-0.5-3.3,1.1-3.5l9.7-1.4c0.7-0.1,1.3-0.5,1.6-1.1l4.3-8.8C20.2,3.1,22.4,3.1,23.2,4.6L23.2,4.6z M24.7,5.9"></path>

                                    </svg>
                                        <div class="mark-name">Mark</div>
                                    </label>
                                </div>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="exam-a__question">
                <h5 class="exam-a__question-title">
                    Question
                    <span class="question-number">
                        {{$try->getCurrentIndex() + 1}} of {{$try->questions()->count()}}
                    </span>
                </h5>
                <p class="question-text">{!! $try->current->question->question !!}</p>
            </div>
            <form action="{{route("user.tests.submit-answer", [
                "product_id" => $productID,
                "try_number" => $tryNumber,
                "question_index" => $try->getCurrentIndex()
            ])}}" class="question-answers__field" id="answer-submit-form" method="post">
                @csrf
                <div class="question-answers">
                    @foreach($try->current->question->variants as $variant)
                        <label class="question-answers-label">
                            <input type="radio" name="answer" value="{{$variant->key}}" class="question-answers-input">
                            <div class="answer">
                                <span class="answer-variant">{{$variant->key}}. </span>
                                {{$variant->answer}}
                            </div>
                        </label>
                    @endforeach
                </div>

                <div class="buttons-field">
                    <label class="prev-button next-button">
                        <input type="submit" value="< prev" name="prev" class="visually-hidden">
                        <strong class="prev-button__strong">&lt;</strong>
                        prev
                    </label>
                    <label class="prev-button">
                        <input type="submit" value="pause" name="pause" class="visually-hidden">
                        pause
                    </label>
                    <label class="prev-button next-button">
                        <input type="submit" value="next >" name="next" class="visually-hidden">
                        next
                        <strong class="prev-button__strong">&gt;</strong>
                    </label>
                </div>
            </form>
        </div>
    </section>
@stop

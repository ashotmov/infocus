@extends("layouts.app")
@section("header-wrapper-class", "exam__header")
@section("footer-wrapper-class", "exam__footer")

@section("content")
    @php
    /** @var App\Models\OnlineTest\TestTry $try */
    @endphp
    <section class="exam-1">
        <div class="container">
            <div class="exam-1__inner">
                <div class="exam-a1__top">
                    <div class="exam-a__top-left">
                        <div class="exam-a__top-title">{{$try->test->name}}</div>
{{--                        <div class="exam-a__top-category">Category: Ethics</div>--}}
                    </div>
                    <a href="{{route("user.dashboard")}}" class="exam-a__top-end">end</a>
                </div>
                <table class="table">
                    <caption>SUMMARY SCORE</caption>
                    <tr class="table-line">
                        <td class="table-line__inner uppercase">DATE STARTED</td>
                        <td class="table-line__inner">{{$try->getStartTime("date")}}</td>
                    </tr>
                    <tr class="table-line">
                        <td class="table-line__inner uppercase">TIME STARTED</td>
                        <td class="table-line__inner">{{$try->getStartTime("time")}}</td>
                    </tr>
                    <tr class="table-line">
                        <td class="table-line__inner uppercase">DATE COMPLETED</td>
                        <td class="table-line__inner">Nov 2, 2018</td>
                    </tr>
                    <tr class="table-line">
                        <td class="table-line__inner uppercase">TIME COMPLETED</td>
                        <td class="table-line__inner">12:13p PT</td>
                    </tr>
                    <tr class="table-line">
                        <td class="table-line__inner uppercase">DURATION</td>
                        <td class="table-line__inner">55:35 mins</td>
                    </tr>
                    <tr class="table-line">
                        <td class="table-line__inner uppercase">ANSWERED</td>
                        <td class="table-line__inner">20</td>
                    </tr>
                    <tr class="table-line">
                        <td class="table-line__inner uppercase">CORRECT</td>
                        <td class="table-line__inner">11</td>
                    </tr>
                    <tr class="table-line">
                        <td class="table-line__inner uppercase">INCORRECT</td>
                        <td class="table-line__inner">9</td>
                    </tr>
                    <tr class="table-line">
                        <td class="table-line__inner uppercase">TOTAL SCORE</td>
                        <td class="table-line__inner">50%</td>
                    </tr>
                </table>
                <table class="table">
                    <caption>SUMMARY SCORE</caption>
                    <tr class="table-line">
                        <td class="table-line__inner uppercase">law</td>
                        <td class="table-line__inner">55%</td>
                    </tr>
                    <tr class="table-line">
                        <td class="table-line__inner uppercase">ethics</td>
                        <td class="table-line__inner">65%</td>
                    </tr>
                </table>
            </div>
        </div>
    </section>
@stop

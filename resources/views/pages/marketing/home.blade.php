@extends("layouts.app")
@section("header-wrapper-class", "wrapper")
@section("footer-wrapper-class", "home__footer")
@section("after-header")
    @include("components.header-slider")
@stop

@section("styles")
    <link rel="stylesheet" href="/extensions/glide/glide.core.min.css">
@stop
@section("scripts")
    <script src="/extensions/glide/glide.min.js"></script>
    <script>
        new Glide('.glide-1').mount();
        new Glide('.glide-2').mount();
    </script>
@stop

@section("content")
    <section class="services__menu">
        <div class="container">
            <div class="services__menu-item">
                <div class="services__menu-top">
                    <h3 class="services__menu-title bottom__line">Services</h3>
                    <p class="services__menu-text">InFocus services are designed to make you a master test-taker (not a
                        therapist). Your confidence and success come from having concrete reading skills and rising
                        scores (not intuition). With personalized tutoring, online courses and in-person group classes,
                        you will learn the techniques for passing the exam. All InFocus services are designed to
                        eliminate the anxiety, the overwhelm and the frustration of test prep by teaching you concrete
                        logic. No more re-reading, overthinking or guessing – you become efficient, confident and
                        pragmatic. The "fog" of studying is replaced with the precision of exam-specific skills. To help
                        you pass the tests, we offer the following:</p>
                </div>
                <ul class="services__icon-field">
                    <li class="services__icons">
                        <a href="#law__ethics" class="services__icon-link">
                            <span class="our-features our-features--online-tests"></span>
                            <p class="services__icon-text">Online Tests</p>
                        </a>
                    </li>
                    <li class="services__icons">
                        <a href="#california__clinica" class="services__icon-link">
                            <span class="our-features our-features--classes"></span>
                            <p class="services__icon-text">Classes</p>
                        </a>
                    </li>
                    <li class="services__icons">
                        <a href="#tutoring" class="services__icon-link">
                            <span class="our-features our-features--tutoring"></span>
                            <p class="services__icon-text">Tutoring</p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </section>
    <section class="services">
        <div id="law__ethics" class="services__inner">
            <div class="container">
                <div class="services__inner-left">
                    <div class="services__img-field">
                        <img class="services__img" src="/img/service-onlinetest.jpg" alt="">
                    </div>
                </div>
                <div class="services__inner-right">
                    <h2 class="services__title">Online Tests</h2>
                    <p class="services__text">Seven (7) Law & Ethics Mock Exams, two (2) Law Focus Exams, and two (2)
                        California Clinical Exams are available. Purchase one at a time. No need to buy more than you
                        need! </p>
                    <div class="services__button-field">
                        <a href="{{route("tests")}}" class="services__button">Learn More</a>
                    </div>
                </div>
            </div>
        </div>
        <div id="california__clinica" class="services__inner">
            <div class="container container-reverse">
                <div class="services__inner-left">
                    <div class="services__img-field services__img-field-reverse">
                        <img class="services__img" src="/img/service-classes.jpg" alt="">
                    </div>
                </div>
                <div class="services__inner-right">
                    <h2 class="services__title">Classes</h2>
                    <p class="services__text">
                        Ask questions and get answers immediately! Go home with succinct hard-copy materials. Learn
                        test-taking strategy in ONE DAY: How to read vignettes ONCE and how to get "unstuck" between
                        two,
                        three or four answers when they all look the same, they all look right or when they all look
                        wrong!
                        We also discuss time management, how to get rid of negative self-dialogue, the skills to
                        eliminate
                        anxiety and overwhelm, how to plot out your mock exams, and what to do the day before and the
                        day-of
                        the exam.
                    </p>
                    <div class="services__button-field">
                        <a href="{{route("classes")}}" class="services__button">Learn More</a>
                    </div>
                </div>
            </div>
        </div>
        <div id="tutoring" class="services__inner">
            <div class="container">

                <div class="services__inner-left">
                    <div class="services__img-field">
                        <img class="services__img" src="/img/service-tutoring.jpg" alt="">
                    </div>
                </div>
                <div class="services__inner-right">
                    <h2 class="services__title">Tutoring</h2>
                    <p class="services__text">Personalized coaching is designed to
                        identify what is getting in the way of raising your score:
                        Getting stuck between two answers, zoning out with long vignettes,
                        anxiety, overthinking, impatience, disagreement with so-called “right” answers, etc. I will
                        train you to eliminate all that self-talk in your head and replace it with a set of concrete
                        reading and logic strategies. You will approach each next mock with
                        greater clarity and more importantly, rising scores.</p>
                    <div class="services__button-field">
                        <a href="{{route("tutoring")}}" class="services__button">Learn More</a>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <section class="management">
        <div class="container">
            <div class="services__menu-top">
                <h3 class="services__menu-title bottom__line">Time and Anxiety Management</h3>
                <p class="services__menu-text">
                    Time and anxiety are related: When you see
                    all the words on the computer
                    screen, you’ll either speed-read, or
                    get bogged down and read too slowly as you try to remember every
                    single word from the vignette. You then get stuck
                    between two answers that both look correct, or four
                    that all look wrong, and then you start re-reading everything all over
                    again. And then you become aware of the time, how frustrated and nervous
                    you are, and then look at the time again, and then you can’t think clearly at all!
                    Test prep starts to consume your life and
                    you resent the entire process.
                </p>
                <p class="services__menu-text"> Here’s the good news: Problems with time management and anxiety are
                    solved by having solid and
                    exam-specific reading skills. Ultimately, anxiety is the result of feeling helpless and lost.
                    But
                    when you have concrete skills, you are neither lost nor helpless. You will know what TO DO
                    (rather
                    than what to “feel”) to get through the each vignette,
                    each answer set, and the entire test efficiently.</p>
            </div>
        </div>
    </section>
    <section class="faqs">
        <div class="container">
            <div class="faqs__inner">
                <div class="faqs__title">
                    <h2 class="faqs__title-top bottom__line">FAQs</h2>
                    <h3 class="faqs__title-bottom">You have questions. We have answers.</h3>
                </div>
                <ul class="faqs__list">
                    <li class="faqs__list-inner">
                        <h3 class="faqs__inner-top">What makes InFocus different?</h3>
                        <p class="faqs__inner-text">
                            I focus almost entirely on exam-specific reading and logic skills. None of your time or
                            money is spent on my telling you how to be a good therapist or trying to connect the exam
                            to your clinical experience.
                            I train you in how to think concretely, efficiently, and pragmatically for the exam.
                        </p>
                    </li>
                    <li class="faqs__list-inner">
                        <h3 class="faqs__inner-top">Where do I start?</h3>
                        <p class="faqs__inner-text">
                            Learn all the material as best you can. Then take a mock exam. Then contact me.
                            That first exam will give us a baseline for your knowledge and your test-taking skills.
                            As we talk through your incorrect answers, I will train you to “think to the exam”.
                        </p>
                    </li>
                </ul>
                <div class="faqs__button-field">
                    <a href="" class="faqs__button">More</a>
                </div>
            </div>
        </div>
    </section>
    <section class="testimonials">
        <div class="container">
            <h3 class="testimonials__title bottom__line">Testimonials</h3>
            <div class="slider-testimonials">
                <div class="glide glide-2">
                    <div data-glide-el="track" class="glide__track">
                        <ul class="glide__slides" data-glide-el="track">
                            <li class="glide__slide">
                                <div class="feedback">
                                    <div class="feedback__inner">
                                        <p class="feedack__inner-text">"I took Gerry Grossman seminars with different
                                            instructors and also purchased materials from Therapist Development Center.
                                            English is not my first language, so in spite of reviewing all the materials
                                            and studying hard, at the time of my exam, I didn't have confidence, I knew
                                            I was not prepared, and my head was cloudy. I took a classroom course for
                                            the second time with Nancy Klein. From the beginning I noticed she was the
                                            one. After couple of private tutoring sessions with her I noticed my head
                                            was clear. She removed a dark cloud from over my head. I knew all the
                                            material but she taught me skills how to differentiate and find the right
                                            answer. Between tutoring sessions, she would answer my texts and emails
                                            regarding any concerns I had with a question that didn't make sense. I wish
                                            I knew her earlier before spending so much time and money. I highly
                                            recommend her for private tutoring."</p>
                                        <h3 class="feedback__inner-title">– Shirin Ghiyam, LMFT</h3>
                                    </div>
                                    <div class="feedback__inner">
                                        <p class="feedack__inner-text">"I met Nancy after failing the first MFT exam. I
                                            had other tutors, study groups, tapes and lectures and flash cards. Studying
                                            under Nancy's instruction was the greatest investment. The KEY difference is
                                            in the critical thinking and observing clues. Nancy has a very unique and
                                            effective way to break down the most significant aspects of the test. I have
                                            no words to thank her for her ability to redirect my thinking in ways I
                                            never would have been able to before. Looking back, I wish I would not have
                                            spent the amount of money in other courses. She will do everything she can
                                            with honesty and sensitivity. I am proof!" "</p>
                                        <h3 class="feedback__inner-title">– Leticia Alexa Thurman, MA, LMFT1</h3>
                                    </div>
                                </div>
                            </li>
                            <li class="glide__slide">
                                <div class="feedback">
                                    <div class="feedback__inner">
                                        <p class="feedack__inner-text">""I took Gerry Grossman seminars with different
                                            instructors and also purchased materials from Therapist Development Center.
                                            English is not my first language, so in spite of reviewing all the materials
                                            and studying hard, at the time of my exam, I didn't have confidence, I knew
                                            I was not prepared, and my head was cloudy. I took a classroom course for
                                            the second time with Nancy Klein. From the beginning I noticed she was the
                                            one. After couple of private tutoring sessions with her I noticed my head
                                            was clear. She removed a dark cloud from over my head. I knew all the
                                            material but she taught me skills how to differentiate and find the right
                                            answer. Between tutoring sessions, she would answer my texts and emails
                                            regarding any concerns I had with a question that didn't make sense. I wish
                                            I knew her earlier before spending so much time and money. I highly
                                            recommend her for private tutoring.</p>
                                        <h3 class="feedback__inner-title">– Shirin Ghiyam, LMFT</h3>
                                    </div>
                                    <div class="feedback__inner">
                                        <p class="feedack__inner-text">"I took Gerry Grossman seminars with different
                                            instructors and also purchased materials from Therapist Development Center.
                                            English is not my first language, so in spite of reviewing all the materials
                                            and studying hard, at the time of my exam, I didn't have confidence, I knew
                                            I was not prepared, and my head was cloudy. I took a classroom course for
                                            the second time with Nancy Klein. From the beginning I noticed she was the
                                            one. After couple of private tutoring sessions with her I noticed my head
                                            was clear. She removed a dark cloud from over my head. I knew all the
                                            material but she taught me skills how to differentiate and find the right
                                            answer. Between tutoring sessions, she would answer my texts and emails
                                            regarding any concerns I had with a question that didn't make sense. I wish
                                            I knew her earlier before spending so much time and money. I highly
                                            recommend her for private tutoring."</p>
                                        <h3 class="feedback__inner-title">– Shirin Ghiyam, LMFT</h3>
                                    </div>
                                </div>
                            </li>
                            <li class="glide__slide">
                                <div class="feedback">
                                    <div class="feedback__inner">
                                        <p class="feedack__inner-text">"I met Nancy after failing the first MFT exam. I
                                            had other tutors, study groups, tapes and lectures and flash cards. Studying
                                            under Nancy's instruction was the greatest investment. The KEY difference is
                                            in the critical thinking and observing clues. Nancy has a very unique and
                                            effective way to break down the most significant aspects of the test. I have
                                            no words to thank her for her ability to redirect my thinking in ways I
                                            never would have been able to before. Looking back, I wish I would not have
                                            spent the amount of money in other courses. She will do everything she can
                                            with honesty and sensitivity. I am proof!" "</p>
                                        <h3 class="feedback__inner-title">– Leticia Alexa Thurman, MA, LMFT1</h3>
                                    </div>
                                    <div class="feedback__inner">
                                        <p class="feedack__inner-text">"I took Gerry Grossman seminars with different
                                            instructors and also purchased materials from Therapist Development Center.
                                            English is not my first language, so in spite of reviewing all the materials
                                            and studying hard, at the time of my exam, I didn't have confidence, I knew
                                            I was not prepared, and my head was cloudy. I took a classroom course for
                                            the second time with Nancy Klein. From the beginning I noticed she was the
                                            one. After couple of private tutoring sessions with her I noticed my head
                                            was clear. She removed a dark cloud from over my head. I knew all the
                                            material but she taught me skills how to differentiate and find the right
                                            answer. Between tutoring sessions, she would answer my texts and emails
                                            regarding any concerns I had with a question that didn't make sense. I wish
                                            I knew her earlier before spending so much time and money. I highly
                                            recommend her for private tutoring."</p>
                                        <h3 class="feedback__inner-title">– Shirin Ghiyam, LMFT</h3>
                                    </div>
                                </div>
                            </li>
                        </ul>
                        <div class="glide__bullets glide__bullets1" data-glide-el="controls[nav]">
                            <button class="glide__bullet glide__bullet1" data-glide-dir="=0"></button>
                            <button class="glide__bullet glide__bullet1" data-glide-dir="=1"></button>
                            <button class="glide__bullet glide__bullet1" data-glide-dir="=2"></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop

@extends("layouts.app")
@section("header-wrapper-class", "contact__header")
@section("footer-wrapper-class", "contact__footer")
@section("after-header")
    <h2 class="online-test">Contact</h2>
@stop

@section("content")
    <section class="get-in-touch__section">
        <div class="container">
            <form action="" class="get-in-touch">
                <fieldset class="get-in-touch__fieldset">
                    <legend class="git-in-touch__legend">
                        Get in touch
                    </legend>
                    <h2 class="get-in-touch__title">Contact us regarding questions</h2>
                    <div class="get-in-touch__row">
                        <input required type="text" class="get-in-touch__input" placeholder="Full Name">
                        <span class="help-block">
                            Name cannot be blank.
                        </span>
                    </div>
                    <div class="get-in-touch__row">
                        <input id="phone-number" required type="tel" pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}"
                               class="get-in-touch__input" placeholder="Phone no.">
                        <span class="help-block">
                            Phone cannot be blank.
                        </span>
                    </div>
                    <div class="get-in-touch__row">
                        <input required type="email" class="get-in-touch__input" placeholder="E-mail address">
                        <span class="help-block">
                            Email cannot be blank.
                        </span>
                    </div>
                    <div class="get-in-touch__row">
                        <input required type="text" class="get-in-touch__input" placeholder="Subject">
                        <span class="help-block">
                            Subject cannot be blank.
                        </span>
                    </div>
                    <div class="get-in-touch__row get-in-touch__row--message">

                        <textarea maxlength="180" class="get-in-touch__message-textarea" id="get-in-touch-message"
                                  placeholder="Message" class="get-in-touch__input"></textarea>
                        <span class="help-block">
                            Message cannot be blank.
                        </span>
                    </div>
                </fieldset>
                <button class="get-in-touch__button">Send message</button>
            </form>
            <div class="get-in-touch__right">
                <div class="mailing__adress">
                    <h3 class="mailing__adress-title">MAILING ADDRESS</h3>
                    <p class="mailing__adress-text">2020 Parkway Street</p>
                    <p class="mailing__adress-text">Los Angeles, CA 90017</p>
                </div>
                <div class="phone__numbers">
                    <h3 class="mailing__adress-title">PHONE NUMBERS</h3>
                    <a href="tel:7603034713" class="phone__numbers-call">(760) 303-4713</a>
                </div>
                <div class="email__adress">
                    <h3 class="mailing__adress-title">EMAIL</h3>
                    <a href="mailto:info@infocus.com" class="phone__numbers-call">info@infocus.com</a>
                </div>
            </div>
        </div>

    </section>
@stop

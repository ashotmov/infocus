@extends("layouts.app")
@section("header-wrapper-class", "tutoring__header")
@section("footer-wrapper-class", "tutoring__footer")
@section("after-header")
    <h2 class="online-test">One-on-One Tutoring</h2>
@stop

@section("content")
    <div class="tutoring-center">
        <section class="tutoring__section">
            <div class="container">
                <div class="tutoring__section-inner">
                    <p class="tutoring__text">Tutoring is uniquely designed to identify YOUR personal challenges in
                        taking mocks and the actual exam.</p>
                    <p class="tutoring__text">We will look at a completed mock exam and focus on your incorrect answers.
                        Those are where your"blind spots' are, or where you spent the most time thinking, over-thinking,
                        and rereading. I will model the strategic reading style you need to read vignettes only once,
                        and how to get "unstuck" between answers.</p>
                    <p class="tutoring__text">I will ask you to tell me what made a vignette or set of answers seem
                        difficult, so I can get "into your head" and train you to "think to the test,"</p>
                    <p class="tutoring__text">In that process, we will develop a set of strategies you can use for each
                        next mock you take. Using these strategies, you then go into the exam with a clear methodology
                        and the confidence that comes from this practical approach.</p>
                    <p class="tutoring__text">Tutoring is also good for evaluating whether you are ready to take the
                        exam. I cannot make guarantees but I will always give you an honest assessment of whether I
                        think you are ready to take your exam on your scheduled date or whether it might be better to
                        reschedule. It's usually better to wait a week or two than four months to retake the exam.</p>
                    <h6 class="tutoring__text-bottom">Don't wait for tutoring until just a few days before your test
                        date. It can take a little bit of time internalize and master how to take the exam in a purely
                        strategic way. Give yourself AT LEAST TWO WEEKS to refine your skills so you pass the first or
                        the next time. Tutoring packages:</h6>
                </div>
            </div>
        </section>
        <section class="session">
            <div class="container">
                <div class="session__inner">
                    <div class="session__first">
                        <img src="/img/tutoring/group-tutoring-img-1552398913.svg" alt="icon" class="session__first-img">
                        <div class="session__text">
                            <div class="session__text-top">Small group tutoring ($100/person/hr)</div>
                            <div class="session__text__bottom">If you and a friend (or two) would like to work together,
                                we
                                can do an in-person session. 7 days/week. Price is for each person.
                            </div>
                        </div>
                    </div>
                    <div class="session__first">
                        <img src="/img/tutoring/conversation.svg" alt="icon" class="session__first-img
    ">
                        <div class="session__text">
                            <div class="session__text-top">In-person session ($125/hr)</div>
                            <div class="session__text__bottom">A 60-minute session. If you prefer face-to-face tutoring,
                                we can go through your exams either in hard copy or on your iPad or laptop. 7 days/week.
                            </div>
                        </div>
                    </div>
                    <div class="session__first">
                        <img src="/img/tutoring/phone-session-img-1552398913.svg" alt="icon" class="session__first-img
    ">
                        <div class="session__text">
                            <div class="session__text-top">Phone session ($100/hr)</div>
                            <div class="session__text__bottom">One 60-minute tutoring session: Learn concrete skills,
                                logic skills and anything else to reduce anxiety, blind spots, or fill in small
                                knowledge gaps. 7 days/week.
                            </div>
                        </div>
                    </div>
                    <a href="tel:7603034713" class="session__button">Call To Schedule A Time!</a>
                </div>
            </div>
        </section>
    </div>
@stop

@extends("layouts.app")
@section("header-wrapper-class", "online-test__header")
@section("footer-wrapper-class", "online-test__footer")
@section("after-header")
    <h2 class="online-test">Online Tests</h2>
@stop

@section("content")
    <section class="exams">
        <div class="exams__container">
            <div class="law__exam">
                <div class="law__exam-icon">
                    <img src="img/tests/law.svg" alt="LAW and ETHICS" class="law__exam-img">
                </div>
                <div class="law__exam-inner">
                    <h2 class="law__exam-title">LAW and ETHICS</h2>
                    <p class="law__exam-text">Seven (7) full-length Law & Ethics Mock Exams (75 questions)</p>
                    <p class="law__exam-text-bottom">Two (2) mini Law Focus Exams (50 questions)</p>
                    <form action="{{route("add-to-cart")}}" method="post">
                        @csrf
                        @foreach($lawTests as $test)

                            <div class="law__exam-input-field">
                                <label class="law__exam-label law__exam-first-label">
                                    <input type="checkbox" class="mock__exam1" name="test[{{$test->id}}]">
                                    <div class="exam__label-check">
                                        <img src="/img/tests/shape_1.png" alt="Checked" class="check__img">
                                    </div>
                                    <div class="mock__exam-field">
                                        <div class="exam__label-text">{{$test->name}}</div>
                                        <div class="exam__label-price">${{$test->price}}</div>
                                    </div>
                                </label>
                            </div>
                        @endforeach
                        {{-- TODO California Exams --}}
                        <div class="law__exam-input-field">
                            <label class="law__exam-label">
                                <input type="checkbox" class="mock__exam3" name="law_package">
                                <div class="exam__label-check">
                                    <img src="/img/tests/shape_1.png" alt="Checked" class="check__img">
                                </div>
                                <div class="mock__exam-field">
                                    <div class="exam__label-text">PACKAGE OF ALL LAW and ETHICS MOCK EXAMS</div>
                                    <div class="exam__label-price">$325</div>
                                </div>

                            </label>
                        </div>
                        <button type="submit" class="law__exam__button">Add To Cart</button>

                    </form>
                </div>
            </div>
            <div class="law__exam clinical-exam">
                <div class="law__exam-icon">
                    <img src="/img/tests/clinical.svg" alt="CALIFORNIA CLINICAL EXAM" class="law__exam-img">
                </div>
                <div class="law__exam-inner">
                    <h2 class="law__exam-title">CALIFORNIA CLINICAL EXAM</h2>
                    <p class="law__exam-text">One (1) full-length California Clinical Exam (170 questions)</p>
                    <p class="law__exam-text-bottom"> Two (2) half-length California Clinical Exams (85 questions)</p>
                    <div class="law__exam-input-field">
                        <label class="law__exam-label">
                            <input type="checkbox" class="clinical__exam" name="checkbox" checked>
                            <div class="exam__label-check">
                                <img src="/img/tests/shape_1.png" alt="Checked" class="check__img">
                            </div>
                            <div class="mock__exam-field">
                                <div class="exam__label-text">California Clinical Mockup Exam 1</div>
                                <div class="exam__label-price">$90</div>
                            </div>

                        </label>
                    </div>
                    <a class="law__exam__button">Add To Cart</a>
                </div>
            </div>
        </div>
    </section>

    <section class="tests__text-field">
        <div class="container">
            <div class="tests__text-field-inner">
                <p class="tests__text">The most critical part of test prep is acquiring the skill of taking the exam
                    itself. And like learning any other skill, repetition is important.</p>
                <p class="tests__text">With your first or second mock exam, you may just be orienting yourself to the
                    process, and:</p>
                <ul class="tests__text-list">
                    <li class="tests__test-list-inner">Your score may not be great.</li>
                    <li class="tests__test-list-inner">You may have taken a long time to finish the exam.</li>
                    <li class="tests__test-list-inner">You may find that what you know from books, classes, flashcards
                        and/or audios doesn't seem to translate into the "correct" answer.
                    </li>
                    <li class="tests__test-list-inner">You may find that what you know from real life client care
                        doesn't seem to translate to the "correct" answer
                    </li>
                    <li class="tests__test-list-inner">You may find that if you are a specialist in an area like
                        substance abuse or domestic violence, those questions seem outright wrong or unrealistic.
                    </li>
                </ul>
                <p class="tests__text">Don't worry at this stage – get tutoring to identify what might have gotten in
                    your way. The sooner you do this, the more you can use your next mock exams to maximum
                    advantage.</p>
                <p class="tests__text">If you already know that anxiety was the problem, this is another good reason to
                    get tutoring early – don't "waste" too many mocks fumbling in the dark. If you can redirect your
                    thinking early, you will save money and time, and prevent the test prep process from making you too
                    self-critical, consuming your daily life, and lasting longer than it needs to.</p>
            </div>
        </div>
    </section>
@stop

@extends("layouts.app")
@section("header-wrapper-class", "classes__header")
@section("footer-wrapper-class", "classes__footer")
@section("after-header")
    <h2 class="online-test">Live Group Classes</h2>
@stop

@section("content")
    <section class="classes__exam-section">
        <div class="exams__container">

            <div class="classes__exam">
                <div class="classes__icon">
                    <span class="our-features our-features--classes"></span>
                </div>
                <div class="classes__exam-inner">
                    <h2 class="law__exam-title classes__Exam-title">CALENDAR OR UPCOMING CLASSES</h2>

                    <div class="classes__exam-input-field">
                        <input type="checkbox" id="classes__exam1" name="checkbox">
                        <label class="law__exam-label classes__first-label" for="classes__exam1">
                            <span class="exam__label-check">
                                <img src="/img/tests/shape_1.png" alt="Checked" class="check__img">
                            </span>
                            <div class="classes__exam-field">
                                <div class="exam__label-text">CCE Mock Exam</div>
                                <div class="exam__label-right">
                                    <div class="classes__label-date">2019-Mar-03</div>
                                    <div class="classes__label-price">$165</div>
                                    <div class="classes__label-clock">09:00 AM - 03:00 PM</div>
                                </div>
                            </div>

                        </label>
                    </div>

                    <div class="classes__exam-input-field">
                        <input type="checkbox" id="classes__exam2" name="checkbox" checked>
                        <label class="law__exam-label classes__first-label" for="classes__exam2">
                            <span class="exam__label-check">
                                <img src="/img/tests/shape_1.png" alt="Checked" class="check__img">
                            </span>
                            <div class="classes__exam-field">
                                <div class="exam__label-text">CCE Mock Exam</div>
                                <div class="exam__label-right">
                                    <div class="classes__label-date">2019-Mar-04</div>
                                    <div class="classes__label-price">$11</div>
                                    <div class="classes__label-clock">11:36 AM - 12:37 AM</div>
                                </div>
                            </div>

                        </label>
                    </div>
                    <div class="classes__exam-input-field">
                        <input type="checkbox" id="classes__exam3" name="checkbox">
                        <label class="law__exam-label classes__first-label" for="classes__exam3">
                            <span class="exam__label-check">
                                <img src="/img/tests/shape_1.png" alt="Checked" class="check__img">
                            </span>
                            <div class="classes__exam-field">
                                <div class="exam__label-text">1-DAY THEORIES and STRATEGIES</div>
                                <div class="exam__label-right">
                                    <div class="classes__label-date">2019-Mar-10</div>
                                    <div class="classes__label-price">$175</div>
                                    <div class="classes__label-clock">09:00 AM - 06:01 PM</div>
                                </div>
                            </div>

                        </label>
                    </div>
                    <div class="classes__exam-input-field">
                        <input type="checkbox" id="classes__exam4" name="checkbox" checked>
                        <label class="law__exam-label classes__first-label" for="classes__exam4">
                            <span class="exam__label-check">
                                <img src="/img/tests/shape_1.png" alt="Checked" class="check__img">
                            </span>
                            <div class="classes__exam-field">
                                <div class="exam__label-text">1-DAY THEORIES and STRATEGIES</div>
                                <div class="exam__label-right">
                                    <div class="classes__label-date">2019-Apr-27</div>
                                    <div class="classes__label-price">$505</div>
                                    <div class="classes__label-clock">10:00 AM - 12:00 PM</div>
                                </div>
                            </div>

                        </label>
                        <p class="classes__bottom-text">Location: 15600 Mulholland Drive, Los Angeles.</p>
                    </div>
                    <a href="#" class="law__exam__button">Add To Cart</a>
                </div>
            </div>


            <div class="cce__exam">
                <h2 class="cce__exam-title"> CCE Mock Exam </h2>
                <p class="cce__exam-text">If you like an in-person learning experience, come take a hard-copy 4-hour
                    exam from 9:00 – 1:00. Test booklet contains rationales for all answers and also serves as a
                    take-home study guide.</p>
                <p class="cce__exam-text">There will be a break from 1:00 – 1:30. Then a strategy session from 1:30 –
                    3:30 (or so). I will teach you all of the techniques to pass the exam:</p>
                <ol class="cce__exam-list">
                    <li class="cce__exam-list-text">How to read the vignette for key words and read it only once
                        (eliminate all that intuition, thinking about "real" clients, re-reading, and trying to remember
                        all the details)
                    </li>
                    <li class="cce__exam-list-text">How to choose the better of two answers when you get stuck
                        (eliminate re-reading, guessing, and overthinking)
                    </li>
                    <li class="cce__exam-list-text">How to manage anxiety and time by replacing all that internal
                        dialogue (that makes long vignettes seem intimidating, theory questions seem impossible, makes
                        you freeze or get frustrated, or like you have to guess or second-guess yourself, etc.) with
                        CONCRETE and LOGICAL EXAM-SPECIFIC techniques
                    </li>
                    <li class="cce__exam-list-text">I will fill in any knowledge gaps or confusion in the content (e.g.,
                        is discussing the limits of confidentiality legal or ethical?, early vs. middle stage theory
                        questions, etc.)
                    </li>
                    <li class="cce__exam-list-text">And I will do my very best to turn you into a pragmatic and
                        efficient test-taker, not a therapist (you are already a therapist!)
                    </li>
                </ol>
            </div>
        </div>
    </section>
    <section class="strategies">
        <div class="strategies__inner">
            <h2 class="strategies__title">1-DAY THEORIES and STRATEGIES</h2>
            <p class="strategies__text">If you are finding all the theories hard to learn and even MORE DIFFICULT to
                translate into correct answers on exam vignettes, this class will strengthen your knowledge AND give you
                real-time experience in answering test questions. We will review all of the theories on the exam:
                Psychodynamic, Object Relations, Family Systems, Cog-B, Narrative, etc. Then we'll do a practice
                question from each theory so you learn how to link your knowledge to specific test-taking strategy
                immediately. For example, we'll talk over Psychodynamic terms and concepts and then do a practice
                vignette. Then we'll talk over Object Relations terms, and do a practice question. You'll also learn how
                to choose goal vs. intervention and early vs. middle so you DON'T HAVE TO MEMORIZE as much information.
                You will receive a hard-copy packet with each theory and a practice test question booklet to take home
                as a study guide.</p>
        </div>
    </section>
@stop

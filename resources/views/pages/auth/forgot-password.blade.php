@extends("layouts.app")
@section("header-wrapper-class", "register__header")
@section("footer-wrapper-class", "register__footer")
@section("after-header")
    <h2 class="online-test">Forgot Password</h2>
@stop

@section("content")

    <section class="register__section">
        <div class="container">
            <div class="register__container">
                <form class="register__form">
                    <label class="register__label">
                        <div class="register__title">Email</div>
                        <input class="register__input" type="text" placeholder="example@example.com">
                        <div class="register__help-block">Email cannot be blank.</div>
                    </label>
                    <div class="sign-in__bottom">
                        <button class="register__button">submit</button>
                    </div>

                </form>
            </div>
        </div>
    </section>
@stop

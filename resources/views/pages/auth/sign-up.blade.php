@extends("layouts.app")
@section("header-wrapper-class", "register__header")
@section("footer-wrapper-class", "register__footer")
@section("after-header")
    <h2 class="online-test">Registration</h2>
@stop

@section("content")
    <section class="register__section">
        <div class="container">
            <div class="register__container">
                <form action="" class="register__form">
                    <label class="register__label">
                        <div class="register__title">First Name<span class="register__title-after">*</span></div>
                        <input class="register__input" type="text" placeholder="John">
                        <div class="register__help-block">First Name cannot be blank.
                        </div>
                    </label>
                    <label class="register__label">
                        <div class="register__title">Last Name<span class="register__title-after">*</span></div>
                        <input class="register__input" type="text" placeholder="Smith">
                        <div class="register__help-block">Last Name cannot be blank.
                        </div>
                    </label>
                    <label class="register__label">
                        <div class="register__title">Username<span class="register__title-after">*</span></div>
                        <input class="register__input" type="text" placeholder="johnsmith">
                        <div class="register__help-block">Username already exist. Please try another one.
                        </div>
                    </label>
                    <label class="register__label">
                        <div class="register__title">Password<span class="register__title-after">*</span></div>
                        <input class="register__input" type="password" placeholder="">
                    </label>
                    <label class="register__label">
                        <div class="register__title">Password (Again)<span class="register__title-after">*</span></div>
                        <input class="register__input" type="password" placeholder="">
                        <div class="register__help-block">Confirm Password cannot be blank.
                        </div>
                    </label>
                    <label class="register__label">
                        <div class="register__title">Degree</div>
                        <input class="register__input" type="password" placeholder="Masters Degree">
                    </label>
                    <label class="register__label">
                        <div class="register__title">Email<span class="register__title-after">*</span></div>
                        <input class="register__input" type="email" placeholder="Example@youmail.com">
                        <div class="register__help-block">Email cannot be blank.
                        </div>
                    </label>
                    <label class="register__label">
                        <div class="register__title">Home Phone</div>
                        <input class="register__input" type="tel" placeholder="(760) 303-4713">
                    </label>
                    <label class="register__label">
                        <div class="register__title">Work Phone</div>
                        <input class="register__input" type="tell" placeholder="(760) 303-4713">
                    </label>
                    <label class="register__label">
                        <div class="register__title">Mobile Phone<span class="register__title-after">*</span></div>
                        <input class="register__input" type="tell" placeholder="(760) 303-4713">
                        <div class="register__help-block">Mobile No cannot be blank.</div>
                    </label>
                    <button class="register__button">register</button>

                </form>
            </div>
        </div>
    </section>
@stop

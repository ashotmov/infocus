@extends("layouts.app")
@section("header-wrapper-class", "register__header")
@section("footer-wrapper-class", "register__footer")
@section("after-header")
    <h2 class="online-test">Sign in</h2>
@stop

@section("content")
    <section class="register__section">
        <div class="container">
            <div class="register__container">
                <form class="register__form" action="{{route("auth.sign-in")}}" method="post">
                    @csrf
                    <label class="register__label">
                        <div class="register__title">Username</div>
                        <input class="register__input" type="text" placeholder="johnsmith" name="username">
                        @error("username")
                        <div class="register__help-block">{{$message}}</div>
                        @enderror
                    </label>
                    <label class="register__label">
                        <div class="register__title">Password</div>
                        <input class="register__input" type="password" placeholder="**********" name="password">
                        @error("password")
                        <div class="register__help-block">{{$message}}</div>
                        @enderror
                    </label>
                    <div class="sign-in__bottom">

                        <button class="register__button" type="submit">sign in</button>
                        <div class="forgot-password">Forgot Password?</div>
                        <a href="{{route("forgot-password")}}" class="click-here">Click Here</a>
                    </div>
                </form>
            </div>
        </div>
    </section>
@stop

<header class="header">
    <div class="container">
        <div class="header__logo">
            <a href="{{route("home")}}" class="logo__link">
                <img src="/img/logo/logo.png" alt="Logo" class="logo">
            </a>
        </div>
        <div class="header__menu">
            <nav class="menu__top">
                <ul class="menu__top-inner">
                    <li class="menu__top-list menu-basket">
                        <a class="menu__top-link basket__img">
                            <span class="goods__pc" id="goods__pc">
                                <span class="goods__pc-numbers">{{count(session("cart", []))}}</span>
                            </span>
                        </a>
                        @php
                            $cartDisplay = session("open-the-cart") ? " popup-active" : "";
                        session()->remove("openCart");
                        @endphp
                        <div class="basket-dropdown{{$cartDisplay}}">
                            <div class="basket-dropdown__top">
                                <div class="basket-dropdown__product">
                                    <h3 class="basket-dropdown__title">Product</h3>
                                </div>
                                <div class="basket-dropdown__price">
                                    <h3 class="basket-dropdown__title">Price</h3>
                                </div>
                                <div class="basket-dropdown__total">
                                    <h3 class="basket-dropdown__title">Total</h3>
                                </div>
                            </div>
                            <div class="basket-dropdown__center">
                                @php
                                    $total = 0;
                                @endphp
                                @foreach(session("cart", []) as $product)
                                    <div class="basket-dropdown1">
                                        <p class="basket-dropdown1__text">{{$product["name"]}}</p>
                                        <p class="basket-dropdown1__price">
                                            {{$product["quantity"]}} x ${{$product["price"]}}
                                        </p>
                                        <p class="basket-dropdown1__total">
                                            @php
                                                $total += $product["price"] * $product["quantity"];
                                            @endphp
                                            ${{$product["price"] * $product["quantity"]}}
                                        </p>
                                        <form action="{{route("remove-from-cart", $product["id"])}}" method="post">
                                            @csrf
                                            @method("delete")
                                            <button type="submit" class="recycle">
                                                <img src="/img/recycle.png" alt="recycle" class="recycle-bin">
                                            </button>
                                        </form>
                                    </div>
                                @endforeach
                                <div class="total-field__right">
                                    <div class="total-total__field">
                                        <p class="total-total">Total</p>
                                        <div class="total-total__price">${{$total}}</div>
                                    </div>
                                </div>
                            </div>
                            <div class="basket-dropdown__bottom">
                                <a href="classes.html" class="continue-ordering">
                                    Continue Ordering
                                    <span class="continue-ordering__line">
                                        <img src="/img/right-arrow.svg" alt="" class="righ">
                                    </span>
                                </a>
                                <a href="{{route("checkout")}}" class="checkout-button">Checkout</a>
                            </div>
                        </div>
                    </li>
                    @guest
                        <li class="menu__top-list">
                            <a href="{{route("sign-in")}}" class="menu__top-link">Sign In</a>
                        </li>
                        <li class="menu__top-list">
                            <a href="{{route("sign-up")}}" class="menu__top-link">Register</a>
                        </li>
                    @else
                        <li class="menu__top-list">
                            <a href="{{route("dashboard")}}" class="menu__top-link">Dashboard</a>
                        </li>
                        <li class="menu__top-list">
                            <a href="{{route("auth.logout")}}" class="menu__top-link logout-button">Logout</a>
                        </li>
                        <form action="{{route("auth.logout")}}" method="post" class="logout-form">
                            @csrf
                        </form>
                    @endguest
                </ul>
            </nav>
            <nav class="menu__field">
                <ul class="menu__field-inner">

                    @foreach($menu as $key => $text)
                        <li class="menu__list">
                            <a href="{{route($key)}}" class="menu__link{{
                            (request()->route()->getName() == $key) ? " active": ""
                            }}">{{$text}}</a></li>
                    @endforeach
                </ul>
            </nav>
        </div>
    </div>
</header>

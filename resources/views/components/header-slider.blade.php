<div class="header__slider">
    <div class="glide glide-1">
        <div class="glide__track" data-glide-el="track">
            <ul class="glide__slides">
                <li class="glide__slide">
                    <div class="container-for-slide">
                        <div class="container">
                            <div class="welcome">
                                <h1 class="welcome-to">Welcome to InFocus Test Prep! </h1>
                                <p class="welcome__text">You are here for one reason: To pass your exams ASAP.</p>
                                <p class="welcome__text">
                                    I am here for one reason: To teach you how.</p>
                                <p class="welcome__text">
                                    InFocus services are designed to make you a master test-taker -- not a therapist
                                    (You are already a therapist!). Here, it's all about a concrete methodology for
                                    reading vignettes, choosing answers, avoiding re-reading and eliminating all the
                                    negative internal dialogue that gets in your way.</p>
                                <p class="welcome__text">
                                    No time with me is spent telling you how to use real clients or your own intuition
                                    to pass the exam -- we discuss ONLY the skills that are relevant to PASSING the
                                    exam.</p>
                                <p class="welcome__text">
                                    So. . .</p>
                                <p class="welcome__text">If you are feeling frustrated, anxious, overwhelmed. . .</p>
                                <p class="welcome__text">
                                    Self-critical, angry, or impatient. . .</p>
                                <p class="welcome__text">
                                    If you've been re-reading</p>

                            </div>
                        </div>
                    </div>
                </li>
                <li class="glide__slide">
                    <div class="container-for-slide">
                        <div class="container">
                            <div class="welcome">
                                <h1 class="welcome-to">Welcome to InFocus Test Prep! 2 </h1>
                                <p class="welcome__text">You are here for one reason: To pass your exams ASAP.</p>
                                <p class="welcome__text">You are here for one reason: To pass your exams ASAP.</p>
                                <p class="welcome__text">You are here for one reason: To pass your exams ASAP.</p>
                                <p class="welcome__text">You are here for one reason: To pass your exams ASAP.</p>
                                <p class="welcome__text">You are here for one reason: To pass your exams ASAP.</p>
                                <p class="welcome__text">You are here for one reason: To pass your exams ASAP.</p>
                                <p class="welcome__text">You are here for one reason: To pass your exams ASAP.</p>


                            </div>
                        </div>
                    </div>
                </li>
                <li class="glide__slide">
                    <div class="container-for-slide">
                        <div class="container">
                            <div class="welcome">
                                <h1 class="welcome-to">Welcome to InFocus Test Prep! 3 </h1>
                                <p class="welcome__text">Welcome to InFocus Test Prep! 3</p>
                                <p class="welcome__text">Welcome to InFocus Test Prep! 3</p>
                                <p class="welcome__text">Welcome to InFocus Test Prep! 3</p>
                                <p class="welcome__text">Welcome to InFocus Test Prep! 3</p>
                                <p class="welcome__text">Welcome to InFocus Test Prep! 3 Welcome to InFocus Test Prep!
                                    3</p>
                                <p class="welcome__text">Welcome to InFocus Test Prep! 3 Welcome to InFocus Test Prep!
                                    3</p>
                                <p class="welcome__text">Welcome to InFocus Test Prep! 3 Welcome to InFocus Test Prep!
                                    3</p>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>

        <div class="glide__bullets" data-glide-el="controls[nav]">
            <button class="glide__bullet" data-glide-dir="=0"></button>
            <button class="glide__bullet" data-glide-dir="=1"></button>
            <button class="glide__bullet" data-glide-dir="=2"></button>
        </div>
    </div>
</div>

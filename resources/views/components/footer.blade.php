<footer class="footer">
    <div class="container">
        <div class="footer__networks">
            <div class="network__list">
                <ul class="network__list-inner">
                    <li class="network__icons">
                        <a href="#" class="network__icons-link">
                            <img src="/img/footer-icons/facebook-logo.svg" alt="icon"
                                 class="network__icons-svg facebook__icon">
                        </a>
                    </li>
                    <li class="network__icons">
                        <a href="#" class="network__icons-link">
                            <img src="/img/footer-icons/twitter.svg" alt="icon"
                                 class="network__icons-svg twitter__icon">
                        </a>
                    </li>
                    <li class="network__icons">
                        <a href="#" class="network__icons-link">
                            <img src="/img/footer-icons/instagram-logo.svg" alt="icon"
                                 class="network__icons-svg instagram__icon">
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="footer__inner">
            <div class="footer__logo">
                <img class="footer__logo-img" src="/img/logo/footer-logo.png" alt="Logo">
            </div>
            <div class="footer__center">
                <nav class="footer__menu-field">
                    <ul class="footer__menu-field-inner">
                        @foreach($menu as $key => $text)
                            <li class="footer__menu-list">
                                <a href="{{route($key)}}"
                                   class="footer__menu-link{{
                                   (request()->route()->getName() == $key) ? " active": ""
                                   }}">{{$text}}</a>
                            </li>
                        @endforeach
                    </ul>
                </nav>
                <p class="reserved">InFocus Test Prep, LLC. All Rights Reserved 2018</p>
            </div>
            <div class="designed">
                <div class="designed__top">Website Designed and Developed</div>
                <div class="designed__bottom">
                    <span class="designed__by">by</span>
                    <a href="#" class="best-draft__link">
                        <img src="/img/logo/best-draft-logo.png" alt="BestDraft" class="designed__img">
                    </a>
                </div>
            </div>
        </div>
    </div>
</footer>

<?php

namespace App;

use App\Models\ECommerce\BillingDetails;
use App\Models\ECommerce\Order;
use App\Models\OnlineTest\Test;
use App\Models\OnlineTest\TestProduct;
use App\Models\OnlineTest\TestTry;
use Illuminate\Database\Eloquent\Model;

/**
 * @property BillingDetails billingDetails
 * @property int id
 * @property int billing_details_id
 */
class UserData extends Model
{
    protected $table = "user_data";
    protected $fillable = [
        "first_name", "last_name", "degree", "home_phone", "work_phone", "mobile_phone"
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function billingDetails()
    {
        return $this->belongsTo(BillingDetails::class, "billing_details_id");
    }

    public function orders()
    {
        return $this->hasMany(Order::class, 'user_id');
        // TODO change field user_id of Order to user_details_id
    }

    public function products()
    {
        return $this->hasMany(TestProduct::class, "user_data_id");
    }
}

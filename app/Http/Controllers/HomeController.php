<?php

namespace App\Http\Controllers;

use App\Models\OnlineTest\Test;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        return view("pages.marketing.home");
    }

    public function tests()
    {
        $lawTests = Test::query()->where("type", Test::LAW_ETHICS)->get();
        return view("pages.marketing.tests", [
            "lawTests" => $lawTests
        ]);
    }

    public function classes()
    {
        return view("pages.marketing.classes");
    }

    public function tutoring()
    {
        return view("pages.marketing.tutoring");
    }

    public function contacts()
    {
        return view("pages.marketing.contacts");
    }
}

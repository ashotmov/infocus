<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    use AuthenticatesUsers;

    public function signInPage()
    {
        return view("pages.auth.sign-in");
    }

    public function signIn(Request $request)
    {
        $this->validate($request, [
            "username" => "required",
            "password" => "required"
        ], [
            "username.required" => "Username cannot be blank.",
            "password.required" => "Password cannot be blank."
        ]);

        if (Auth::attempt(
            $request->only("username", "password"),
            $request->has("remember")
        )) {
            return redirect()->route("admin.dashboard");
        }

        return redirect()->back()
            ->withInput($request->only("username", "remember"))
            ->withErrors([
                "password" => "Incorrect username or password.",
            ]);
    }

    public function signUpPage()
    {
        return view("pages.auth.sign-up");
    }

    public function signUp(Request $request)
    {
        $this->validate($request, [
            "first_name" => "required",
            "last_name" => "required",
            "username" => "required|unique:users",
            "email" => "required|email|unique:users",
            "password" => "required|confirmed|min:6",
            "degree" => "nullable",
            "home_phone" => "nullable|regex:/^[(]{1}[0-9]{3}[)]{1} [0-9]{3}-[0-9]{4}$/i",
            "work_phone" => "nullable|regex:/^[(]{1}[0-9]{3}[)]{1} [0-9]{3}-[0-9]{4}$/i",
            "mobile_phone" => "required|regex:/^[(]{1}[0-9]{3}[)]{1} [0-9]{3}-[0-9]{4}$/i"
        ]);

        $user = User::query()->create($request->all());
    }

    public function forgotPasswordPage()
    {
        return view("pages.auth.forgot-password");
    }

    public function signOut(Request $request)
    {
        if (Auth::check()) {
            $this->logout($request);
        }

        return redirect()->route('home');
    }
}

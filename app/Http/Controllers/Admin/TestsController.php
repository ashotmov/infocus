<?php

namespace App\Http\Controllers\Admin;

use App\Models\OnlineTest\Test;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TestsController extends Controller
{
    public function index()
    {
        $tests = Test::all();
        return view("pages.admin.tests.index", [
            "tests" => $tests
        ]);
    }

    public function show($id)
    {
        $questionNumber = request()->query("question");

        /** @var Test $test */
        $test = Test::query()->findOrFail($id);
        $question = $test->questions()->first() ?? null;
        $previous = null;
        $next = @$test->questions[1]->number;

        if ($questionNumber) {
            for ($i = 0; $i < $test->questions()->count(); $i++) {
                if ($test->questions[$i]->number == $questionNumber) {
                    $question = $test->questions[$i];
                    $previous = @$test->questions[$i - 1]->number;
                    $next = @$test->questions[$i + 1]->number;
                }
            }
        }

        return view("pages.admin.tests.show", [
            "test" => $test,
            "question" => $question,
            "previous" => $previous,
            "next" => $next
        ]);
    }

    public function delete($id)
    {
        $test = Test::query()->findOrFail($id);
        $test->delete();
        return redirect()->back();
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Models\OnlineTest\Question;
use App\Models\OnlineTest\Test;
use App\Models\OnlineTest\Variant;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function index()
    {
        return view("pages.admin.dashboard");
    }

    public function testSheetUrl(Request $request, $type)
    {
        $this->validate($request, [
            "url" => [
                "required",
                "regex:/^https:\/\/docs\.google\.com\/spreadsheets\/d\/[a-zA-Z0-9_-]+/"
            ],
        ]);

        preg_match(
            '/^https:\/\/docs\.google\.com\/spreadsheets\/d\/([a-zA-Z0-9_-]+)/',
            $request->get("url"),
            $matches
        );

        list(, $sheetId) = $matches;

        $url = "https://docs.google.com/spreadsheets/d/$sheetId/export?format=zip";

        try {
            $fileContent = file_get_contents($url);
        } catch (\Exception $e) {
            return redirect()->back()->withErrors([
                "url" => "Cannot get file."
            ]);
        }

        file_put_contents(storage_path('app/file.zip'), $fileContent);

        $zip = new \ZipArchive();
        if ($zip->open(storage_path('app/file.zip'))) {
            $zip->extractTo(storage_path('app/law/'));
            $zip->close();
            unlink(storage_path('app/file.zip'));
        }

        $files = scandir(storage_path('app/law'));

        $parser = [
            0 => "number",
            1 => "category",
            2 => "answer",

            4 => "question",

            5 => "answer_A",
            6 => "answer_B",
            7 => "answer_C",
            8 => "answer_D",

            9 => "rationale_A",
            10 => "rationale_B",
            11 => "rationale_C",
            12 => "rationale_D"
        ];

        foreach ($files as $file) {
            if (preg_match('/^E/', $file)) {

                $test = new Test();
                $test->name = "Law & Ethics Mock " . explode('.', $file)[0];
                $test->type = Test::LAW_ETHICS;
                $test->price = 30;
                $test->save();

                $dom = new \DOMDocument();
                $dom->loadHTMLFile(storage_path('app/law/' . $file));
                $tbody = $dom->getElementsByTagName("tbody")->item(0);
                $trs = $tbody->getElementsByTagName("tr");
                for ($i = 2; $i < $trs->count(); $i++) {
                    $tr = $trs->item($i);
                    $tds = $tr->getElementsByTagName("td");
                    $data = [];
                    if (!@$tds->item(0)->firstChild->data) {
                        continue;
                    }
                    foreach ($parser as $index => $key) {
                        $data[$key] = @$tds->item($index)->firstChild->data;
                    }

                    if (!$data["number"] || !$data["category"] || !$data["question"] || !$data["answer"]) {
                        continue;
                    }

                    $question = new Question($data);
                    $test->questions()->save($question);

                    $vp = ["A", "B", "C", "D"];

                    foreach ($vp as $key) {
                        if (!$data["answer_$key"] || !$data["rationale_$key"]) {
                            continue;
                        }
                        $variant = new Variant([
                            "key" => $key,
                            "answer" => $data["answer_$key"],
                            "rationale" => $data["rationale_$key"]
                        ]);
                        $question->variants()->save($variant);
                    }
                }
            }
        }

        return redirect()->route("admin.tests");
    }
}

<?php

namespace App\Http\Controllers\User;

use App\Models\OnlineTest\Test;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CartController extends Controller
{
    public function addToCart(Request $request)
    {
        $testIDs = array_keys($request->get("test") ?? []);

        $tests = Test::query()->whereIn("id", $testIDs)->get();

        $cart = $request->session()->get("cart", []);

        foreach ($tests as $test) {
            $index = -1;
            for ($i = 0; $i < count($cart); $i++) {
                if ($cart[$i]["id"] == $test->id) {
                    $index = $i;
                    break;
                }
            }

            if ($index === -1) {
                $cart[] = [
                    "id" => $test->id,
                    "quantity" => 1,
                    "price" => $test->price,
                    "name" => $test->name
                ];
            } else {
                $cart[$index]["quantity"]++;
            }
        }

        $request->session()->put("cart", $cart);

//        $request->session()->flush();

        if ($request->wantsJson()) {
            return [
                "cart" => $cart
            ];
        } else {
            return redirect()->back()->with("open-the-cart", true);
        }
    }

    public function removeFromCart(Request $request, $id)
    {
        $cart = $request->session()->get("cart", []);

        for ($i = 0; $i < count($cart); $i++) {
            if ($cart[$i]["id"] == $id) {
                unset($cart[$i]);
                break;
            }
        }

        $cart = array_values($cart);

        $request->session()->put("cart", $cart);
        $request->session()->put("openCart", true);

        return redirect()->back();
    }
}

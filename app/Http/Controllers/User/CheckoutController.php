<?php

namespace App\Http\Controllers\User;

use App\Models\ECommerce\BillingDetails;
use App\Models\ECommerce\Order;
use App\Models\OnlineTest\Test;
use App\Models\OnlineTest\TestProduct;
use App\Models\OnlineTest\TestTry;
use App\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\ValidationException;

class CheckoutController extends Controller
{
    public function index()
    {
        $user = User::query()->find(auth()->user()->id);
        return view("pages.e-commerce.checkout", [
            "user" => $user
        ]);
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     * @throws ValidationException
     */
    public function saveAddress(Request $request)
    {
        $this->validate($request, [
            "first_name" => "required",
            "last_name" => "required",
            "address" => "required",
            "additional_address" => "nullable",
            "town_city" => "required",
            "postcode" => "nullable",
            "email" => "required|email",
            "phone" => "required|regex:/^[(]{1}[0-9]{3}[)]{1} [0-9]{3}-[0-9]{4}$/i",
            "order_notes" => "nullable"
        ]);

        /** @var User $user */
        $user = User::query()->find(auth()->user()->id);

        $billingDetails = new BillingDetails($request->all());
        $billingDetails->save();
        $user->data->billing_details_id = $billingDetails->id;
        $user->data->save();
        $user->save();

        return redirect()->back()->with("success", "Billing details saved successful!");
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     * @throws ValidationException
     */
    public function order(Request $request)
    {
        $this->validate($request, [
            "agree" => "required"
        ]);

        /** @var User $user */
        $user = User::query()->find(auth()->user()->id);

        $billingDetails = $user->data->billingDetails;

        if (!$billingDetails) {
            return redirect()->back()->withInput()->withErrors([
                "billing_details" => "Please set billing address before ordering."
            ]);
        }

        $cart = $request->session()->get("cart", []);
        $total = 0;
        $ids = [];

        foreach ($cart as $product) {
            $ids[] = $product['id'];
        }

        $tests = Test::query()->find($ids);

        if (count($cart) == 0) {
            return redirect()->back()->withInput()->withErrors([
                "cart" => "Cart is empty."
            ]);
        }

        // TODO Payment
        // if ok

        /**
         * @var Order $order
         */
        $order = new Order([
            "title" => $cart[0]["name"] . (count($cart) > 1 ? ", ..." : ""),
            "total" => $total
        ]);

        $order->user_id = $user->data->id;
        $order->billing_details_id = $user->data->billingDetails->id;
        $order->save();

        /** @var Test $test */
        foreach ($tests as $test) {

            /** @var TestProduct $testProduct */
            $testProduct = new TestProduct([
                "test_id" => $test->id
            ]);

            $testProduct->user_data_id = $user->data->id;
            $testProduct->order_id = $order->id;

            /**
             * @var TestTry $try1
             * @var TestTry $try2
             */
            $try1 = new TestTry();
            $try1->test_id = $test->id;

            $try2 = new TestTry();
            $try2->test_id = $test->id;

            $try1->save();
            $try2->save();

            $testProduct->try_1 = $try1->id;
            $testProduct->try_2 = $try2->id;

            $testProduct->save();
        }

        $request->session()->remove("cart");

        return redirect()->route("user.dashboard");
    }
}

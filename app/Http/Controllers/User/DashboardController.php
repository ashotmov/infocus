<?php

namespace App\Http\Controllers\User;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function index()
    {
        $user = User::query()->find(auth()->user()->id);

        return view("pages.user.dashboard", [
            "user" => $user
        ]);
    }
}

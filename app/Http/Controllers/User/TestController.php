<?php

namespace App\Http\Controllers\User;

use App\Models\OnlineTest\TestTry;
use App\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use App\Models\OnlineTest\TestProduct;
use Illuminate\Validation\ValidationException;

class TestController extends Controller
{
    /**
     * @param Request $request
     * @return RedirectResponse
     * @throws ValidationException
     */
    public function startTest(Request $request)
    {
        $this->validate($request, [
            "try_number" => "required",
            "product_id" => "required",
            "mode" => "required"
        ]);

        /** @var User $user */
        $user = User::query()->find(auth()->user()->id);

        /** @var TestProduct $product */
        $product = $user->data->products()->find($request->get("product_id"));

        if (!$product) {
            return redirect()->route("user.dashboard");
        }


        /** @var TestTry $try */
        $try = null;

        switch ($request->get("try_number")) {
            case 1:
                $try = $product->try1;
                break;
            case 2:
                $try = $product->try2;
                break;
            default:
                return redirect()->route("user.dashboard");
        }

        $try->reset();

        $try->start($request->get("mode"));

//        dd(Carbon::parse($try->start_time)->diff(Carbon::now()));
        // TODO continue

        return redirect()->route("user.tests.show", [
            "product_id" => $product->id,
            "try_number" => $request->get("try_number")
        ]);
    }

    public function continueTest()
    {

    }

    public function reviewTest()
    {

    }

    /**
     * @param $product_id
     * @param $try_number
     * @return RedirectResponse
     * @throws ModelNotFoundException
     */
    public function showTest($product_id, $try_number)
    {
        /** @var User $user */
        $user = User::query()->find(auth()->user()->id);
        /** @var TestProduct $product */
        $product = $user->data->products()->findOrFail($product_id);
        /** @var TestTry $try */
        $try = null;


        switch ($try_number) {
            case 1:
                $try = $product->try1;
                break;
            case 2:
                $try = $product->try2;
                break;
        }

        if (!$try->isStarted()) {
            return redirect()->route("user.dashboard");
        }

        $try->updateTime();

        $data = [
            "try" => $try,
            "productID" => $product_id,
            "tryNumber" => $try_number
        ];

        if ($try->is_exam) {
            return view("pages.tests.exam", $data);
        } else {
            return view("pages.tests.study", $data);
        }
    }

    /**
     * @param Request $request
     * @param int $product_id
     * @param int $try_number
     * @param int $question_index
     * @return RedirectResponse
     * @throws ModelNotFoundException
     * @throws ValidationException
     */
    public function submitAnswer(Request $request, $product_id, $try_number, $question_index)
    {
        $this->validate($request, [
            "answer" => "nullable",
            "star" => "nullable"
        ]);

        /**
         * @var User $user
         * @var TestProduct $product
         * @var TestTry $try
         */
        $user = User::query()->find(auth()->user()->id);
        $product = $user->data->products()->findOrFail($product_id);
        $try = null;

        if ($try_number == 1) {
            $try = $product->try1;
        } else {
            $try = $product->try2;
        }

        $try->setCurrent($question_index);

        $try->saveAnswer($request->get("answer"), $request->has("star"));

        if ($request->has("prev")) {
            $try->previous();
        } elseif ($request->has("pause")) {
            $try->pause();
            return redirect()->route("user.dashboard");
        } elseif ($request->has("next")) {
            $try->next();
        } elseif ($request->has("to_number")) {
            $try->go($request->get("question_number"));
        } elseif ($request->has("prev_star")) {
            $try->previousStar();
        } elseif ($request->has("next_star")) {
            $try->nextStar();
        } elseif ($request->has("to_first")) {
            $try->go(1);
        } elseif ($request->has("to_last")) {
            $try->go(1000);
        } elseif ($request->has("end")) {
            $try->end();

            return redirect()->route("user.tests.result", [
                "product_id" => $product_id,
                "try_number" => $try_number
            ]);
        }

        return redirect()->route("user.tests.show", [
            "product_id" => $product_id,
            "try_number" => $try_number
        ]);
    }

    public function result($product_id, $try_number)
    {

        /**
         * @var User $user
         * @var TestProduct $product
         * @var TestTry $try
         */
        $user = User::query()->find(auth()->user()->id);
        $product = $user->data->products()->findOrFail($product_id);
        $try = null;

        if ($try_number == 1) {
            $try = $product->try1;
        } else {
            $try = $product->try2;
        }


        return view("pages.tests.result", ["try" => $try]);
    }
}

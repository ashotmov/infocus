<?php

namespace App\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        view()->share("menu",  [
            "home" => "Home",
            "tests" => "Tests",
            "classes" => "Classes",
            "tutoring" => "Tutoring",
            "contacts" => "Contact"
        ]);
    }
}

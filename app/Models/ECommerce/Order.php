<?php

namespace App\Models\ECommerce;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int user_id
 * @property int billing_details_id
 * @property int id
 */
class Order extends Model
{
    protected $table = "orders";
    protected $fillable = [
        "title", "total"
    ];

    public function products()
    {
        return $this->hasMany(OrderProduct::class, 'order_id');
    }
}

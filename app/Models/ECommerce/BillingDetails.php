<?php

namespace App\Models\ECommerce;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer id
 */
class BillingDetails extends Model
{
    protected $table = "billing_details";
    protected $fillable = [
      "first_name", "last_name", "address", "additional_address",
      "town_city", "postcode", "email", "phone", "order_notes"
    ];
}

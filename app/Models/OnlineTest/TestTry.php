<?php

namespace App\Models\OnlineTest;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed end_time
 * @property mixed start_time
 * @property mixed continue_time
 * @property int user_data_id
 * @property int test_id
 * @property int id
 * @property bool is_pause
 * @property int spent_time
 * @property Collection questions
 * @property Test test
 * @property bool is_exam
 * @property int current_id
 * @property int length
 * @property TestTryQuestion current
 */
class TestTry extends Model
{
    protected $table = "test_tries";
    protected $fillable = [
        "spent_time", "is_pause"
    ];

    public function test()
    {
        return $this->belongsTo(Test::class, "test_id");
    }

    public function questions()
    {
        return $this->hasMany(TestTryQuestion::class, "try_id");
    }

    public function current()
    {
        return $this->belongsTo(TestTryQuestion::class, "current_id");
    }

    public function isStarted()
    {
        return $this->start_time != null;
    }

    public function isExamStarted()
    {
        return $this->is_exam && !!$this->start_time;
    }

    public function isCompleted()
    {
        return $this->end_time != null;
    }

    public function getTimeLeft()
    {
        $time = $this->length - $this->spent_time;

        return (int)($time / 3600) . " hours " . (int)($time % 3600 / 60) . " mins";
    }

    public function getStartTime($mode)
    {
        switch ($mode) {
            case "date":
                return Carbon::createFromTimeString($this->start_time)->format("d M Y");
            case "time":
                return Carbon::createFromTimeString($this->start_time)->format("h:i A");
            default:
                return Carbon::createFromTimeString($this->start_time)->toDateTimeString();
        }
    }

    public function getResult()
    {
        // TODO calculate result of try if try is completed and return "null" on the other case
        return "10%";
    }

    public function reset()
    {
        if ($this->is_exam) {
            return;
        }

        $this->is_pause = false;
        $this->start_time = null;
        $this->end_time = null;
        $this->spent_time = 0;
        $this->continue_time = null;
        $this->save();

        foreach ($this->questions as $question) {
            $question->delete();
        }

        foreach ($this->test->questions as $question) {

            $tryQuestion = new TestTryQuestion();
            $tryQuestion->try_id = $this->id;
            $tryQuestion->question_id = $question->id;
            $tryQuestion->save();
        }

        $this->current_id = $this->questions()->first()->id;
        $this->save();
    }

    public function start($mode)
    {
//        if ($this->is_exam) {
//            return;
//        }

//        dump($mode);

        switch ($mode) {
            case "exam":
                $this->is_exam = true;
                $this->start_time = Carbon::now()->toDateTimeString();
                $this->continue_time = Carbon::now()->toDateTimeString();
                $this->end_time = null;
                $this->is_pause = false;
                $this->spent_time = 0;
                break;
            case "study":
                $this->is_exam = false;
                $this->start_time = Carbon::now()->toDateTimeString();
                $this->continue_time = null;
                $this->end_time = null;
                $this->is_pause = false;
                $this->spent_time = 0;
                break;
        }

        $this->save();
    }

    public function pause()
    {
        if (!$this->is_exam) {
            return;
        }

        $this->is_pause = true;
        $this->save();
    }

    public function getCurrentIndex()
    {
        for ($i = 0; $i < $this->questions()->count(); $i++) {
            if ($this->questions[$i]->id == $this->current->id) {
                return $i;
            }
        }
        return 0;
    }

    public function setCurrent($index)
    {
        $this->current_id = @$this->questions[$index]->id;
        $this->save();
    }

    public function isCurrentLast()
    {
        return $this->getCurrentIndex() == $this->questions()->count() - 1;
    }

    public function isCurrentFirst()
    {
        return $this->getCurrentIndex() == 0;
    }

    public function updateTime()
    {
        $this->spent_time += Carbon::now()
                ->timestamp
            - Carbon::createFromTimeString($this->continue_time)
                ->timestamp;
        $this->continue_time = Carbon::now()->toDateTimeString();
        $this->save();
    }

    public function saveAnswer($key, $star)
    {
        $this->current->stared = !!$star;

        $this->current->variant_id =
            @$this->current->question->variants()->where("key", $key)->first()->id;

        $this->current->save();
    }

    public function next()
    {
        $this->updateTime();

        if ($this->isCurrentLast()) {
            $this->current_id = $this->questions()->first()->id;
        }

        $this->current_id = $this->questions[$this->getCurrentIndex() + 1]->id;

        $this->save();
    }

    public function previous()
    {
        $this->updateTime();

        if ($this->isCurrentFirst()) {
            return;
        }

        $this->current_id = $this->questions[$this->getCurrentIndex() - 1]->id;

        $this->save();
    }

    public function nextStar()
    {
        $this->updateTime();

        for ($i = $this->getCurrentIndex() + 1; $i < $this->questions()->count(); $i++) {
            if ($this->questions[$i]->stared) {
                $this->go($i + 1);
                break;
            }
        }
    }

    public function previousStar()
    {
        $this->updateTime();

        for ($i = $this->getCurrentIndex() - 1; $i >= 0; $i--) {
            if ($this->questions[$i]->stared) {
                $this->go($i + 1);
                break;
            }
        }
    }

    public function go($number)
    {
        $index = $number - 1;
        if ($index < 0) {
            $index = 0;
        }
        if ($index >= $this->questions()->count()) {
            $index = $this->questions()->count() - 1;
        }

        $this->current_id = $this->questions[$index]->id;
        $this->save();
    }

    public function end()
    {
        $this->end_time = Carbon::now()->toTimeString();
        $this->is_pause = false;
        $this->save();
    }
}

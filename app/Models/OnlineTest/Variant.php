<?php

namespace App\Models\OnlineTest;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string key
 */
class Variant extends Model
{
    protected $table = "variants";
    protected $fillable = [
        "key", "answer", "rationale"
    ];

    public function question()
    {
        return $this->belongsTo(Question::class, "question_id");
    }
}

<?php

namespace App\Models\OnlineTest;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * @property float price
 * @property integer id
 * @property Collection questions
 */
class Test extends Model
{
    public const LAW_ETHICS = 1;
    public const CALIFORNIA = 2;

    protected $table = "tests";
    protected $fillable = [
        "name", "type", "price"
    ];

    public function questions()
    {
        return $this->hasMany(Question::class, "test_id");
    }
}

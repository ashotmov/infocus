<?php

namespace App\Models\OnlineTest;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int order_id
 * @property int try_1
 * @property int try_2
 * @property int user_data_id
 * @property TestTry try1
 * @property TestTry try2
 * @property int id
 */
class TestProduct extends Model
{
    protected $table = "test_products";
    protected $fillable = [
        "test_id", "try_1", "try_2"
    ];

    public function test()
    {
        return $this->belongsTo(Test::class, "test_id");
    }

    public function try1()
    {
        return $this->belongsTo(TestTry::class, "try_1");
    }

    public function try2()
    {
        return $this->belongsTo(TestTry::class, "try_2");
    }
}

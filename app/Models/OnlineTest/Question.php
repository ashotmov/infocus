<?php

namespace App\Models\OnlineTest;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string answer
 * @property string question
 * @property mixed variants
 */
class Question extends Model
{
    protected $table = "questions";
    protected $fillable = [
        "number", "category", "question", "answer"
    ];

    public function test()
    {
        return $this->belongsTo(Test::class, "test_id");
    }

    public function variants()
    {
        return $this->hasMany(Variant::class, "question_id");
    }
}

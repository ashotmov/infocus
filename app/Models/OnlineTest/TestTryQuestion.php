<?php

namespace App\Models\OnlineTest;

use Illuminate\Database\Eloquent\Model;

/**
 * @property Variant variant
 * @property Question question
 * @property int question_id
 * @property int try_id
 * @property int variant_id
 * @property boolean stared
 * @property int id
 */
class TestTryQuestion extends Model
{
    protected $table = "test_try_questions";
    protected $fillable = [
        "stared"
    ];

    public function question()
    {
        return $this->belongsTo(Question::class, 'question_id');
    }

    public function variant()
    {
        return $this->belongsTo(Variant::class, 'variant_id');
    }

    public function isRight()
    {
        return @$this->variant->key == $this->question->answer;
    }
}

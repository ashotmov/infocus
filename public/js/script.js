window.addEventListener('load', () => {
    const logoutFormSubmit_a = document.querySelector('a.logout-button');
    const logoutForm = document.querySelector('form.logout-form');

    if (logoutFormSubmit_a) {
        logoutFormSubmit_a.addEventListener("click", e => {
            e.preventDefault();
            logoutForm.submit();
        });
    }

    const phoneInputs = document.querySelectorAll('input.phone-input');
    for (const input of Array.from(phoneInputs)) {
        input.value = normalizeNumber(input.value);
        input.addEventListener('input', function () {
            this.value = normalizeNumber(this.value);
        });
    }

    const cart_a = document.querySelector("a.menu__top-link.basket__img");
    const cart_dropdown = document.querySelector("div.basket-dropdown");

    if (cart_a && cart_dropdown) {
        cart_a.addEventListener("click", (e) => {
            if (cart_dropdown.classList.contains("popup-active")) {
                cart_dropdown.classList.remove("popup-active");
            } else {
                cart_dropdown.classList.add("popup-active");
            }
            e.preventDefault();
        });
    }


    // User Dashboard
    (function () {
        const wrapper = document.querySelector(".wrapper-popup");
        const choosePopup = document.querySelector(".popup-choose-exam");

        const startButtons = document.querySelectorAll("button.start-test-button");

        for (const button of Array.from(startButtons)) {
            button.addEventListener("click", () => {
                openChoosePopup(button.dataset);
            });
        }

        function openChoosePopup({number, productId}) {
            if (wrapper && choosePopup) {
                choosePopup.classList.add("popup-active");
                wrapper.classList.add("popup-active");

                const cancelButton = choosePopup.querySelector("button.choose-exam__cancel");
                const form = choosePopup.querySelector("form.choose-exam__form");

                const numberInput = form.querySelector("input.choose-form__number");
                const productIdInput = form.querySelector("input.choose-form__product-id");

                numberInput.value = number;
                productIdInput.value = productId;

                cancelButton.addEventListener("click", closeChoosePopup, {once: true});
                wrapper.addEventListener("click", closeChoosePopup, {once: true});
            }
        }

        function closeChoosePopup() {
            if (wrapper && choosePopup) {
                choosePopup.classList.remove("popup-active");
                wrapper.classList.remove("popup-active");
            }
        }
    })();

});

function normalizeNumber(value) {
    const numbers = value.replace(/\D/g, '').split('');

    numbers.splice(10);
    if (numbers.length > 6) {
        numbers.splice(6, 0, '-');
    }

    if (numbers.length > 3) {
        numbers.splice(3, 0, ') ');
        numbers.splice(0, 0, '(');
    }

    return numbers.join('');
}

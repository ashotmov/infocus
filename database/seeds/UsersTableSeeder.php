<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'username' => "admin",
            'email' => "admin@infocus.co",
            'is_admin' => true,
            'password' => bcrypt('111'),
        ]);
        DB::table('users')->insert([
            'username' => "user",
            'email' => "user@infocus.co",
            'is_admin' => false,
            'password' => bcrypt('111'),
        ]);
        DB::table('user_data')->insert([
            'user_id' => 2,
            'first_name' => "John",
            'last_name' => "Doe",
//            'degree' => "",
//            'home_phone' => "",
//            'work_phone' => "",
            'mobile_phone' => "7603034713",
        ]);
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTestTryQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("test_try_questions", function (Blueprint $table) {
            $table->bigIncrements("id");
            $table->bigInteger("try_id");
            $table->bigInteger("question_id");
            $table->bigInteger("variant_id")->nullable();
            $table->boolean("stared")->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('test_try_questions');
    }
}

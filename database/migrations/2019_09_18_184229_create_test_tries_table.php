<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTestTriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("test_tries", function (Blueprint $table) {
            $table->bigIncrements("id");
            $table->bigInteger("test_id");
            $table->bigInteger("current_id")->nullable();
            $table->timestamp("start_time")->nullable();
            $table->timestamp("end_time")->nullable();
            $table->timestamp("continue_time")->nullable();

            $table->integer("spent_time")->default(0);
            $table->integer("length")->default(60 * 60 * 2);
            $table->boolean("is_pause")->default(false);
            $table->boolean("is_exam")->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('test_tries');
    }
}

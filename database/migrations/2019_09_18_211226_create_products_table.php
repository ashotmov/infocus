<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("test_products", function (Blueprint $table) {
            $table->bigIncrements("id");
            $table->bigInteger("user_data_id");
            $table->bigInteger("order_id");
            $table->bigInteger("test_id");
            $table->bigInteger("try_1")->nullable();
            $table->bigInteger("try_2")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('test_products');
    }
}
